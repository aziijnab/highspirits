import React, { Component } from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';



import Home from "../Screens/Home/Home";
import Calculation from '../Screens/Home/Calculation';
import SalesTeamCalculation from '../Screens/Home/SalesTeamCalculation';
import UnitEconomics from '../Screens/Home/UnitEconomics';
import table from '../Screens/Home/table';
import showPdf from '../Screens/Home/showPDF';
import mainPage from '../Screens/Home/mainPage';
const Stack = createStackNavigator();

export default function AppNavigator() {
  return (
    <NavigationContainer>
    <Stack.Navigator initialRouteName="mainPage" headerMode={"none"}>
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="UnitEconomics" component={UnitEconomics} />
      <Stack.Screen name="Calculation" component={Calculation} />
      <Stack.Screen name="SalesTeamCalculation" component={SalesTeamCalculation} />
      <Stack.Screen name="table" component={table} />
      <Stack.Screen name="showPdf" component={showPdf} />
      <Stack.Screen name="mainPage" component={mainPage} />
     
      </Stack.Navigator>
  </NavigationContainer> 
  );
}
