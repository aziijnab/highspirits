import React, { Component } from 'react';
import {
  View, Text, TouchableOpacity, Image, StyleSheet, Picker, Dimensions, ImageBackground, TextInput, Linking} from 'react-native';
import { Ionicons, EvilIcons, MaterialIcons , Entypo,FontAwesome5,AntDesign} from '@expo/vector-icons';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import {URL} from '../constants/API'


export default class SideBar extends Component {
  // const [started, setStarted] = useState(false)
  constructor(props) {
    super(props);
    this.state = {
       started: false,
  
    }

  }
 

  render() {
    return (

      <View style={{ flex: 1, alignItems: "center",backgroundColor:"#FFFFEE" }}>
<View style={{width:"100%",marginTop:40,backgroundColor:"",paddingLeft:25,alignItems:"center",flexDirection:"row"}}>
<View style={{width:"30%",alignItems:"center",justifyContent:"center",}}>
<Image style={{width:"100%",height:50,resizeMode:"stretch",borderRadius:90}} source={require('../assets/images/9.jpg')}/>
</View>
<View style={{width:"50%",marginHorizontal:12}}>
<Text style={{fontSize:22,fontWeight:"bold",color:"#FF5733",fontStyle:"italic"}}>Jump In Shape</Text>
</View>
</View>
<TouchableOpacity style={{width:"90%",marginTop:40,paddingBottom:10,paddingLeft:15,borderBottomWidth:1,borderColor:"#E3E3E3"}} onPress={()=>this.props.navigation.navigate('login')}>
<Text style={{fontSize:18,fontWeight:"bold",color:"#656565"}}>Sign In</Text>
</TouchableOpacity>
<TouchableOpacity style={{width:"90%",marginTop:20,paddingBottom:10,paddingLeft:15,borderBottomWidth:1,borderColor:"#E3E3E3"}} onPress={()=>this.props.navigation.navigate('profile')} >
<Text style={{fontSize:18,fontWeight:"bold",color:"#656565"}}>Profile In</Text>
</TouchableOpacity>
<TouchableOpacity style={{width:"90%",marginTop:20,paddingBottom:10,paddingLeft:15,borderBottomWidth:1,borderColor:"#E3E3E3"}} onPress={()=>this.props.navigation.navigate('learn')} >
<Text style={{fontSize:18,fontWeight:"bold",color:"#656565"}}>Learn</Text>
</TouchableOpacity>
<TouchableOpacity style={{width:"90%",marginTop:20,paddingBottom:10,paddingLeft:15,borderBottomWidth:1,borderColor:"#E3E3E3"}} onPress={()=>this.props.navigation.navigate('settings')} >
<Text style={{fontSize:18,fontWeight:"bold",color:"#656565"}}>Settings</Text>
</TouchableOpacity>
   <View style={{width:"90%",marginTop:20,paddingBottom:10,paddingLeft:15,borderBottomWidth:1,borderColor:"#E3E3E3"}} onPress={()=>this.props.navigation.navigate()} >
<View>
<Text style={{fontSize:18,fontWeight:"bold",color:"#656565"}}>Follow Us</Text>
</View>
<View style={{width:"100%",flexDirection:"row",marginTop:12}}>
  <TouchableOpacity style={{width:"15%"}}  onPress={ ()=>{ Linking.openURL('https://facebook.com')}}>
  <FontAwesome5 name="facebook" size={24} color="grey" />
  </TouchableOpacity>
  <TouchableOpacity style={{width:"15%"}} onPress={ ()=>{ Linking.openURL('https://instagram.com')}}>
  <AntDesign name="instagram" size={24} color="grey" />
  </TouchableOpacity>
  <TouchableOpacity style={{width:"15%"}} onPress={ ()=>{ Linking.openURL('https://youtube.com')}} >
  <AntDesign name="youtube" size={24} color="grey" />
  </TouchableOpacity>
</View>
</View>   
<TouchableOpacity style={{width:"90%",marginTop:20,paddingBottom:10,paddingLeft:15,borderBottomWidth:1,borderColor:"#E3E3E3"}} onPress={ ()=>{ Linking.openURL('https://www.crossrope.com/pages/terms-and-conditions')}} >
<Text style={{fontSize:18,fontWeight:"bold",color:"#656565"}}>Terms of use </Text>
</TouchableOpacity>
<TouchableOpacity style={{width:"90%",marginTop:20,paddingBottom:10,paddingLeft:15,borderBottomWidth:1,borderColor:"#E3E3E3"}} onPress={ ()=>{ Linking.openURL('https://www.crossrope.com/pages/privacy-policy')}} >
<Text style={{fontSize:18,fontWeight:"bold",color:"#656565"}}>Privacy Policy</Text>
</TouchableOpacity>
      </View>
      

    );
  }

}
const styles = StyleSheet.create({
  pic: {


    height: 250,
    width: "100%",


  },
  login_button: {
    width: "90%",

    marginTop: 10,
    paddingVertical: 13,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#808080",
    borderRadius: 10
  },
  facebook: {

    width: "90%",

    marginTop: 30,
    paddingVertical: 13,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#005f9A",
    borderRadius: 10
  },
});