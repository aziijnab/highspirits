import React, { Component } from "react";
import {
  StyleSheet,
  StatusBar,
  Text,
  View,
  Dimensions,
  AsyncStorage,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
  ScrollView,
} from "react-native";
import Constants from "expo-constants";

import { Container, Picker, Input, Button } from "native-base";
import RNPickerSelect from "react-native-picker-select";
import { Ionicons } from "@expo/vector-icons";
import { URL } from "../../components/API";
import BaseHeader from "../../components/BaseHeader/Header.js";
import { Notifications } from "expo";
import Modal from "react-native-modal";
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class table extends Component {
  constructor(props) {
    super(props);
  
    this.state = {
      showData: "Ladder #9",
      brand: "",
      type: "",
      bottle_size: "",
      abv: "",
      retail_price_p_bottle: "",
      case_size: "",
      no_of_pallet: "",
      case_per_pallet: "",
      retail_margin: "",
      distributor_margin: "",
      domestic_transportation: "",
      ap_support: "",
      pbc: "",
    };
    this.data = this.props.route.params;
    console.log("data")
    console.log(this.data.calculations)
    console.log(this.data.brand)
  }
  componentDidMount() {}

  render() {
    const { navigation } = this.props.navigation;
    return (
      <View
        style={{
          width: deviceWidth,
          height: deviceHeight,
          backgroundColor:"#003616",
          marginTop: Constants.statusBarHeight,
        }}
      >
        <View style={{ width: "100%", height: "100%" }}>
        <BaseHeader
            navigation={this.props.navigation}
            logo={true}
            back={true}
          />
          {this.state.loading ? (
            <View
              style={{
                width: deviceWidth,
                height: deviceHeight,
                alignContent: "center",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <ActivityIndicator size={"large"} />
            </View>
          ) : (
            <ScrollView>
              <View
                style={{
                  width: "100%",
                  alignItems: "center",
                  alignContent: "center",
                }}
              >
                <View style={{ width: "90%", paddingBottom: 200 }}>
                  <Text style={{ color: "#ffffff" }}>Version 1.0 | 2020</Text>
                  <View
                    style={{
                      width: "100%",
                      marginTop: 5,
                      alignContent: "center",
                      alignItems: "center",
                    }}
                  >
<View style={{width:"100%", borderColor:"#fff", borderWidth:0.5, flexDirection:"row"}}>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
 
  </View>
  <View style={{width:"66.66%"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:8}}>
      <Text style={{fontSize:15, fontWeight:"bold", color:"#fff"}}>
      {this.data.brand}	 </Text>
    </View>
  </View>

</View>
<View style={{width:"100%", borderColor:"#fff", borderWidth:0.5, flexDirection:"row"}}>
  <View style={{width:"33.33%"}}></View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, fontWeight:"bold", color:"#fff"}}>
      Bottle	 </Text>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, fontWeight:"bold", color:"#fff"}}>
      Case	 </Text>
    </View>
  </View>
</View>

<View style={{width:"100%", borderColor:"#fff", borderWidth:0.5, flexDirection:"row"}}>
  <View style={{width:"33.33%"}}></View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      {this.data.calculations.bottle_size}mL	 </Text>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      {this.data.calculations.case_size}	 </Text>
    </View>
  </View>
</View>

<View style={{width:"100%", borderColor:"#fff", borderWidth:0.5, flexDirection:"row"}}>
<View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
     	 </Text>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      ${this.data.calculations.retail_price_per_bottle} </Text>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      ${this.data.calculations.case_price_per_bottle} 
	 </Text>
    </View>
  </View>
</View>

<View style={{width:"100%", borderColor:"#fff", borderWidth:0.5, flexDirection:"row"}}>
<View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      Retail Margin%	 </Text>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      {this.data.calculations.bottle_retail_margin} %  </Text>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      {this.data.calculations.case_retail_margin} %
	 </Text>
    </View>
  </View>
</View>

<View style={{width:"100%", borderColor:"#fff", borderWidth:0.5, flexDirection:"row"}}>
<View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      Retail margin $
	 </Text>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      ${parseFloat(this.data.calculations.bottle_retail_margin_in_dollar).toFixed(2) }  
 </Text>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      ${parseFloat(this.data.calculations.case_retail_margin_in_dollar).toFixed(2) }
	 </Text>
    </View>
  </View>
</View>

<View style={{width:"100%", borderColor:"#fff", borderWidth:0.5, flexDirection:"row"}}>
<View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      Price to Retailer

	 </Text>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      ${parseFloat(this.data.calculations.bottle_price_to_retailer).toFixed(2) } 
 </Text>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      ${parseFloat(this.data.calculations.case_price_to_retailer).toFixed(2) } 
	 </Text>
    </View>
  </View>
</View>

<View style={{width:"100%", borderColor:"#fff", borderWidth:0.5, flexDirection:"row"}}>
<View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      Distributor Margin %


	 </Text>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      {this.data.calculations.distributor_margin} %
 </Text>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      {this.data.calculations.distributor_margin} %
	 </Text>
    </View>
  </View>
</View>

<View style={{width:"100%", borderColor:"#fff", borderWidth:0.5, flexDirection:"row"}}>
<View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:8}}>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:8}}>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:8}}>
    </View>
  </View>
</View>

<View style={{width:"100%", borderColor:"#fff", borderWidth:0.5, flexDirection:"row"}}>
<View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      Distributor Margin $


	 </Text>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      ${parseFloat(this.data.calculations.bottle_distributor_margin_in_dollar).toFixed(2) } 
 </Text>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      ${parseFloat(this.data.calculations.case_distributor_margin_in_dollar).toFixed(2) } 
	 </Text>
    </View>
  </View>
</View>

<View style={{width:"100%", borderColor:"#fff", borderWidth:0.5, flexDirection:"row"}}>
<View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      State Excise Tax - (Example TX)


	 </Text>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      ${parseFloat(this.data.calculations.bottle_state_excise_tax).toFixed(2) } 
      
 </Text>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      ${parseFloat(this.data.calculations.case_state_excise_tax).toFixed(2) } 
	 </Text>
    </View>
  </View>
</View>


<View style={{width:"100%", borderColor:"#fff", borderWidth:0.5, flexDirection:"row"}}>
<View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      Domestic Transportation


	 </Text>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      ${parseFloat(this.data.calculations.bottle_domestic_transportation).toFixed(2) } 
 </Text>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      ${parseFloat(this.data.calculations.case_domestic_transportation).toFixed(2) }
	 </Text>
    </View>
  </View>
</View>


<View style={{width:"100%", borderColor:"#fff", borderWidth:0.5, flexDirection:"row"}}>
<View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:8}}>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:8}}>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:8}}>
    </View>
  </View>
</View>


<View style={{width:"100%", borderColor:"#fff", borderWidth:0.5, flexDirection:"row"}}>
<View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      FOB/ Price to Distributor


	 </Text>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      ${parseFloat(this.data.calculations.bottle_fob).toFixed(2) }
 </Text>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      ${parseFloat(this.data.calculations.case_fob).toFixed(2) }
	 </Text>
    </View>
  </View>
</View>


<View style={{width:"100%", borderColor:"#fff", borderWidth:0.5, flexDirection:"row"}}>
<View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:8}}>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:8}}>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:8}}>
    </View>
  </View>
</View>


<View style={{width:"100%", borderColor:"#fff", borderWidth:0.5, flexDirection:"row"}}>
<View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      FET


	 </Text>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      ${parseFloat(this.data.calculations.bottle_fet).toFixed(2) }

 </Text>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      ${parseFloat(this.data.calculations.case_fet).toFixed(2) } 
	 </Text>
    </View>
  </View>
</View>

<View style={{width:"100%", borderColor:"#fff", borderWidth:0.5, flexDirection:"row"}}>
<View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      A&P Support


	 </Text>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      ${parseFloat(this.data.calculations.bottle_ap_support).toFixed(2) } 
 </Text>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      ${parseFloat(this.data.calculations.case_ap_support).toFixed(2) } 
	 </Text>
    </View>
  </View>
</View>

<View style={{width:"100%", borderColor:"#fff", borderWidth:0.5, flexDirection:"row"}}>
<View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      Net FOB Origin




	 </Text>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      ${parseFloat(this.data.calculations.bottle_net_fob_origin).toFixed(2) } 
 </Text>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      ${parseFloat(this.data.calculations.case_net_fob_origin).toFixed(2) } 
	 </Text>
    </View>
  </View>
</View>
{this.data.calculations.case_pbc_marginal_profit ? (
  <View>
<View style={{width:"100%", borderColor:"#fff", borderWidth:0.5, flexDirection:"row"}}>
<View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      PBC Marginal profit




	 </Text>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      ${parseFloat(this.data.calculations.bottle_pbc_marginal_profit).toFixed(2) } 
 </Text>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      ${parseFloat(this.data.calculations.case_pbc_marginal_profit).toFixed(2) } 
	 </Text>
    </View>
  </View>
</View>

<View style={{width:"100%", borderColor:"#fff", borderWidth:0.5, flexDirection:"row"}}>
<View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      PBC Marginal profit margin
 </Text>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      ${parseFloat(this.data.calculations.bottle_pbc_marginal_profit_margin).toFixed(2) } 
 </Text>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      ${parseFloat(this.data.calculations.case_pbc_marginal_profit_margin).toFixed(2) } 
	 </Text>
    </View>
  </View>
</View>

<View style={{width:"100%", borderColaor:"#fff", borderWidth:0.5, flexDirection:"row"}}>
<View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      PBC Origina Cost - OZT Ex-works
 </Text>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      ${parseFloat(this.data.calculations.bottle_pbc_original_cost).toFixed(2) }  
 </Text>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      ${parseFloat(this.data.calculations.case_pbc_original_cost).toFixed(2) }  
	 </Text>
    </View>
  </View>
</View>

</View>


) : null}

<View style={{flexDirection:"row", marginTop:50}}>
<View style={{width:"50%", alignContent:"center", alignItems:"center"}}>
  <View style={{width:"95%",borderColor:"#fff", borderWidth:0.5}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4, borderWidth:0.5, borderColor:"#fff"}}>
      <Text style={{fontSize:15, color:"#fff"}}>
     Hard FOB
 </Text>
    </View>
    <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      ${parseFloat(this.data.calculations.hard_fob).toFixed(2) }
      
 </Text>
    </View>
  </View>
</View>
<View style={{width:"50%", alignContent:"center", alignItems:"center"}}>
<View style={{width:"95%",borderColor:"#fff", borderWidth:0.5}}>
<View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4, borderWidth:0.5, borderColor:"#fff"}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      HARD EX-WORKS
 </Text>
    </View>
    <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff"}}>
      ${parseFloat(this.data.calculations.hard_ex_works).toFixed(2) }
 </Text>
    </View>
  </View>
</View>

  
  
</View>

     {/* ..............................             */}
<View style={{width:"100%", borderColor:"#fff", borderWidth:0.5, flexDirection:"row"}}>
<View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff", fontWeight:"bold"}}>
      {this.data.brand}





	 </Text>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
  <Text style={{fontSize:12, color:"#fff", fontWeight:"bold"}}>
  HARD EX-WORKS (No FET)

</Text>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
  <Text style={{fontSize:12, color:"#fff", fontWeight:"bold"}}>
  HARD FOB (w/FET)
</Text>
    </View>
  </View>
</View>
  {/* ..............................             */}
<View style={{width:"100%", borderColor:"#fff", borderWidth:0.5, flexDirection:"row"}}>
<View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
<View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff", fontWeight:"bold"}}>
      750 ml





	 </Text>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
  <Text style={{fontSize:12, color:"#fff", fontWeight:"bold"}}>
  ${parseFloat(this.data.calculations.hard_fob).toFixed(2) }

</Text>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
  <Text style={{fontSize:12, color:"#fff", fontWeight:"bold"}}>
  ${parseFloat(this.data.calculations.hard_ex_works).toFixed(2) }
</Text>
    </View>
  </View>
</View>
 {/* ..............................             */}
<View style={{width:"100%", borderColor:"#fff", borderWidth:0.5, flexDirection:"row"}}>
<View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
<View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
      <Text style={{fontSize:15, color:"#fff", fontWeight:"bold"}}>
      1.75 ml





	 </Text>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
  <Text style={{fontSize:12, color:"#fff", fontWeight:"bold"}}>
  ${parseFloat(this.data.calculations.hard_175_fob).toFixed(2) }

</Text>
    </View>
  </View>
  <View style={{width:"33.33%", borderWidth:0.5, borderColor:"#fff"}}>
  <View style={{width:"100%", alignContent:"center", alignItems:"center", justifyContent:"center",paddingVertical:4}}>
  <Text style={{fontSize:12, color:"#fff", fontWeight:"bold"}}>
  ${parseFloat(this.data.calculations.hard_175_ex_works).toFixed(2) }
</Text>
    </View>
  </View>
</View>

 </View>
                </View>
              </View>
            </ScrollView>
          )}
        </View>
      </View>
    );
  }
}
const smallpickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 8,
    paddingLeft: 7,

    color: "#fff",
    width: "100%",
    // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 16,
    paddingVertical: 8,
    paddingLeft: 7,

    color: "#fff",
    width: "100%", // to ensure the text is never behind the icon
  },
});
export default table;
