import React, { Component } from "react";
import {
  StyleSheet,
  StatusBar,
  Text,
  Button,
  View,
  Dimensions,
  AsyncStorage,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
  ScrollView,
} from "react-native";
import Constants from "expo-constants";

import { Container, Picker } from "native-base";
import { URL } from "../../components/API";
import BaseHeader from "../../components/BaseHeader/Header.js";
import PDFReader from 'rn-pdf-reader-js'
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;
class showPdf extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lob: 1,
      bottled_in: 1,
      distilate_id: 3,
      terrepure_finished: 1,
      flavoured: 0,
      fet: 0,
      packaging: 2,
      bottle: 4,
      bottles_per_case: 12,
      closure: 3,
      label_config: 3,
      additional: 0,
      bottle_size: 5,
      bottling_tier_id: 4,
      packaging_tier_id: 0,
      p_tier: 2,
      total_price: 0,
      discount_price: 0,
      discount_percent: 10,
      isModalVisible: false,
      distillates_data: [],
      bottles_data: [],
      bottle_sizes_data: [],
      loading: true,
      loading_result: true,
    };
    this.data = this.props.route.params;
   
  }
  
  componentDidMount() {
    
   
  }

  render() {
    const { navigation } = this.props.navigation;
    return (
      <View
      style={{
          width: deviceWidth,
          height: deviceHeight,
          backgroundColor:"#003616",
          marginTop: Constants.statusBarHeight,
        }}
    >
 <View style={{ width: "100%", height: "100%" }}>
          <BaseHeader
            navigation={this.props.navigation}
            logo={true}
            back={true}
          />
         {/* <PDFReader
        source={{
          uri: 'http://winery.brewerydtc.com/public/assets/css/'+this.data.file_name,
        }}
      /> */}
      <PDFReader
        source={{
          uri: 'http://winery.brewerydtc.com/public/assets/css/'+this.data.file_name,
        }}
      />
        </View>
       
    </View>
       
    );
  }
}

export default showPdf;
