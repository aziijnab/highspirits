//import React from 'react';
import React, { Component } from "react";
import {
  StyleSheet,
  StatusBar,
  Text,
  View,
  Dimensions,
  AsyncStorage,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
  ScrollView
} from "react-native";
import Constants from "expo-constants";

import { Container, Picker } from "native-base";
import { URL } from "../../components/API";
import BaseHeader from "../../components/BaseHeader/Header.js";
import { Notifications } from "expo";
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      caps: 0,
      corks: 0,
      bottles: 0,
      labels: 0,
      // caps_data: [],
      corks_data: [],
      bottles_data: [],
      labels_data: [],
      total:0,
      loading:true,
    };
  }
  componentDidMount() {
    this.fetchData();
  }
  fetchData = () => {
    fetch(URL + "get-data", {
      method: "GET"
    })
      .then(res => res.json())
      .then(async response => {
        if (response.response == "success") {
          this.setState({
            // caps_data: response.caps_data,
            // corks_data: response.corks_data,
            bottles_data: response.bottles_data,
            // labels_data: response.labels_data,
            loading : false
          });
        } else {
          Alert.alert(
            "Note",
            "Please Check Internet Connection. Thanks",
            [{ text: "OK" }],
            { cancelable: true }
          );
        }
      })
      .catch(error => alert("Please Check Your Internet Connection"));
  };
  onCapSelect(value) {
    console.log(value)
    var total = 0;
    if(value == 0.00){
total = parseFloat(this.state.total)  - parseFloat(this.state.caps);
    }
    else{
      total = parseFloat(this.state.total) + parseFloat(value);
    }
    this.setState({
      caps: value,
      total: total
    });
  }
  onCorkSelect(value) {
    console.log(value)
    var total = 0;
    if(value == 0.00){

total = parseFloat(this.state.total) - parseFloat(this.state.corks) ;
console.log("total = 0 = >")
console.log(total)
    }
    else{
      total = parseFloat(this.state.total) + parseFloat(value) ;
      console.log("total =else = >")
console.log(total)
    }
    
    this.setState({
      corks: value,
      total: total
    });
  }
  onBottleSelect(value) {
    console.log(value)
    var total = 0;
    if(value == 0.00){
total = parseFloat(this.state.total) - parseFloat(this.state.bottles);
    }
    else{
      total = parseFloat(this.state.total) + parseFloat(value);
    }
    this.setState({
      bottles: value,
      total: total
    });
  }
  onLabelSelect(value) {
    console.log(value)
    var total = 0;
    if(value == 0.00){
total = parseFloat(this.state.total) - parseFloat(this.state.labels);
    }
    else{
      total = parseFloat(this.state.total) + parseFloat(value);
    }
    this.setState({
      labels: value,
      total: total
    });
  }
  render() {
    const { navigation } = this.props.navigation;
    const fontColor = "#82a601";
    if(this.state.loading)
    return (
         <View style={{ width: deviceWidth, height: deviceHeight, alignContent:"center", alignItems:"center", justifyContent:"center" }}>
<ActivityIndicator size={"large"}/>
         </View>
    );
    else
    return (
      <Container>
        <View style={{ width: deviceWidth, height: deviceHeight }}>
          <BaseHeader
            navigation={this.props.navigation}
            title={"Winery App"}
            logo={true}
          />
          <ScrollView>
            <View
              style={{
                width: "100%",
                alignItems: "center",
                alignContent: "center"
              }}
            >
              <View style={{ width: "90%", paddingBottom: 200 }}>
                <Text style={{ color: "#808080" }}>Version 1.0 | 2020</Text>
                <View style={{ marginTop: 20 }}>
                  <Text style={{ fontSize: 20, fontWeight: "bold" }}>
                    Select Cap
                  </Text>
                </View>
                <View
                  style={{
                    width: "100%",
                    borderWidth: 0.8,
                    borderRadius: 10,
                    borderColor: "#808080",
                    padding: 0,
                    marginTop: 10
                  }}
                >
                  <Picker
                    mode="dropdown"
                    style={{ width: undefined, height: 40 }}
                    placeholder="Caps"
                    placeholderStyle={{ color: "#808080" }}
                    placeholderIconColor="#808080"
                    selectedValue={this.state.caps}
                    onValueChange={this.onCapSelect.bind(this)}
                  >
                    <Picker.Item label="Select" value="0.00" />
                    <Picker.Item
                          label={'Dark'}
                          value={'2.40'}
                        />
                    {/* {this.state.caps_data.length > 0 ? 
                      this.state.caps_data.map((item, index) => {
                      return (
                        <Picker.Item
                          key={index}
                          label={item.cap_name}
                          value={item.price}
                        />
                      );
                    })
                     : null } */}

                  </Picker>
                </View>

                <View style={{ marginTop: 20 }}>
                  <Text style={{ fontSize: 20, fontWeight: "bold" }}>
                    Select Cork
                  </Text>
                </View>
                <View
                  style={{
                    width: "100%",
                    borderWidth: 0.8,
                    borderRadius: 10,
                    borderColor: "#808080",
                    padding: 0,
                    marginTop: 10
                  }}
                >
                  <Picker
                    mode="dropdown"
                    style={{ width: undefined, height: 40 }}
                    placeholder="Cork"
                    placeholderStyle={{ color: "#808080" }}
                    placeholderIconColor="#808080"
                    selectedValue={this.state.corks}
                    onValueChange={this.onCorkSelect.bind(this)}
                  >
                    <Picker.Item label="Select" value="0.00" />
                    <Picker.Item
                          label={'Neelum'}
                          value={'12.44'}
                        />
                    {/* {this.state.corks_data.map((item, index) => {
                      return (
                        <Picker.Item
                          key={index}
                          label={item.cork_name}
                          value={item.price}
                        />
                      );
                    })} */}
                  </Picker>
                </View>

                <View style={{ marginTop: 20 }}>
                  <Text style={{ fontSize: 20, fontWeight: "bold" }}>
                    Select Bottle
                  </Text>
                </View>
                <View
                  style={{
                    width: "100%",
                    borderWidth: 0.8,
                    borderRadius: 10,
                    borderColor: "#808080",
                    padding: 0,
                    marginTop: 10
                  }}
                >
                  <Picker
                    mode="dropdown"
                    style={{ width: undefined, height: 40 }}
                    placeholder="Cork"
                    placeholderStyle={{ color: "#808080" }}
                    placeholderIconColor="#808080"
                    selectedValue={this.state.bottles}
                    onValueChange={this.onBottleSelect.bind(this)}
                    
                  >
                    <Picker.Item label="Select" value="0.00" />
                    {this.state.bottles_data.map((item, index) => {
                      return (
                        <Picker.Item
                          key={index}
                          label={item.bottle_name}
                          value={item.price}
                        />
                      );
                    })}
                  </Picker>
                </View>

                <View style={{ marginTop: 20 }}>
                  <Text style={{ fontSize: 20, fontWeight: "bold" }}>
                    Select Label
                  </Text>
                </View>
                <View
                  style={{
                    width: "100%",
                    borderWidth: 0.8,
                    borderRadius: 10,
                    borderColor: "#808080",
                    padding: 0,
                    marginTop: 10
                  }}
                >
                  <Picker
                    mode="dropdown"
                    style={{ width: undefined, height: 40 }}
                    placeholder="Cork"
                    placeholderStyle={{ color: "#808080" }}
                    placeholderIconColor="#808080"
                    selectedValue={this.state.labels}
                    onValueChange={this.onLabelSelect.bind(this)}
                  >
                      <Picker.Item
                          label={"Private"}
                          value={"Private"}
                        />
                    {/* {this.state.labels_data.map((item, index) => {
                      return (
                        <Picker.Item
                          key={index}
                          label={item.label_name}
                          value={item.price}
                        />
                      );
                    })} */}
                  </Picker>
                </View>
                
                <View
                  style={{
                    width: "100%",
                    alignContent: "center",
                    alignItems: "center",
                    marginTop: 30
                  }}
                >
                  <Text style={{ fontSize: 26, fontWeight: "bold" }}>
                    Estimated Total
                  </Text>
                </View>
                <View
                  style={{
                    width: "100%",
                    alignContent: "center",
                    alignItems: "center",
                    marginTop: 30
                  }}
                >
                  <View
                    style={{
                      width: "50%",
                      padding: 10,
                      borderRadius: 20,
                      borderWidth: 0.8,
                      borderColor: "#808080",
                      alignItems: "center",
                      alignContent: "center"
                    }}
                  >
                    <Text style={{ fontSize: 26, fontWeight: "bold" }}>
                      $ {parseFloat(this.state.total).toFixed(2) }
                    </Text>
                  </View>
                </View>

                {/* <View style={{flexDirection:"row", marginTop:30}}>
     <View>
    <Text style={{fontSize:26, fontWeight:"bold"}}>Total :</Text>
  </View>
  <View style={{marginLeft:"auto"}}>
    <Text style={{fontSize:24, fontWeight:"bold"}}>$ 155.00</Text>
  </View>
  <View>
  </View>
     </View> */}
              </View>
            </View>
          </ScrollView>
        </View>
      </Container>
    );
  }
}
export default Home;
