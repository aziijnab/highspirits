import React, { Component } from "react";
import {
  StyleSheet,
  StatusBar,
  Text,
  View,
  Dimensions,
  AsyncStorage,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
  ScrollView,
} from "react-native";
import Constants from "expo-constants";

import { Container, Picker, Input, Button } from "native-base";
import RNPickerSelect from "react-native-picker-select";
import { Ionicons } from "@expo/vector-icons";
import { URL } from "../../components/API";
import BaseHeader from "../../components/BaseHeader/Header.js";
import { Notifications } from "expo";
import Modal from "react-native-modal";
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class mainPage extends Component {
  constructor(props) {
    super(props);
  
    this.state = {
      
    };
  }
  componentDidMount() {}
  onBrandSelect(value) {
    console.log("value");
    console.log(value);
this.props.navigation.navigate('showPdf',{
  file_name: value
})
  }
  render() {
    const { navigation } = this.props.navigation;
    const placeholder_brands = {
      label: "Brands...",
      value: null,
      color: "#eaeaea",
      key: 0
    };
    return (
      <View
        style={{
          width: deviceWidth,
          height: deviceHeight,
          backgroundColor:"#003616",
          marginTop: Constants.statusBarHeight,
        }}
      >
        <View style={{ width: "100%", height: "100%" }}>
        <BaseHeader
            navigation={this.props.navigation}
            logo={true}
          />
          {this.state.loading ? (
            <View
              style={{
                width: deviceWidth,
                height: deviceHeight,
                alignContent: "center",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <ActivityIndicator size={"large"} />
            </View>
          ) : (
            <View
                style={{
                  width: "100%",
                  height: "100%",
                  alignItems: "center",
                  alignContent: "center",
                }}
              >
                <View style={{ width: "90%" }}>
                  <Text style={{ color: "#ffffff" }}>Version 1.0 | 2020</Text>
                  <View
                    style={{
                      width: "100%",
                      marginTop: 5,
                      alignContent: "center",
                      alignItems: "center",
                    }}
                  >


                  </View>
                </View>
                <View style={{width:"80%", alignContent:"center", alignItems:"center", marginTop:50}}>

                <Button
                    bordered
                    onPress={() => this.props.navigation.navigate('Calculation')}
                    style={{
                      width: "100%",
                      height:80,
                      alignItems: "center",
                      justifyContent: "center",
                      borderColor: "#fff",
                      borderRadius: 5,
                      backgroundColor: "#000"
                    }}
                  >
                    <Text
                      style={{
                        textAlign: "center",
                        color: "#e8eaf6",
                        fontSize: 17,
                        fontWeight:"bold"
                      }}
                    >
                      Calculation
                    </Text>
                  </Button>
<View style={{paddingVertical:10}}></View>
                  <Button
                    bordered
                    onPress={() => this.props.navigation.navigate('UnitEconomics')}
                    style={{
                      width: "100%",
                      height:80,
                      alignItems: "center",
                      justifyContent: "center",
                      borderColor: "#fff",
                      borderRadius: 5,
                      backgroundColor: "#000"
                    }}
                  >
                    <Text
                      style={{
                        textAlign: "center",
                        color: "#e8eaf6",
                        fontSize: 17,
                        fontWeight:"bold"
                      }}
                    >
                    Distributor Unit Economics
                    </Text>
                  </Button>
<View style={{paddingVertical:10}}></View>
                  <Button
                    bordered
                    onPress={() => this.props.navigation.navigate('SalesTeamCalculation')}
                    style={{
                      width: "100%",
                      height:80,
                      alignItems: "center",
                      justifyContent: "center",
                      borderColor: "#fff",
                      borderRadius: 5,
                      backgroundColor: "#000"
                    }}
                  >
                    <Text
                      style={{
                        textAlign: "center",
                        color: "#e8eaf6",
                        fontSize: 17,
                        fontWeight:"bold"
                      }}
                    >
                    Indicative Pricing Template by Tier
                    </Text>
                  </Button>
                  <View style={{width:"100%", marginTop:20, alignContent:"center", alignItems:"center"}}>
                    {/* <Button onPress={()=> this.props.navigation.navigate('showPdf')} color="#fff" title="Brands"/> */}
                    <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          backgroundColor: "#000",
                          marginTop: 10,
                          height:80,
                      justifyContent: "center",
                      borderColor: "#fff",
                      borderRadius: 5,


                        }}
                      >
                     
                        <RNPickerSelect
                        placeholder={placeholder_brands}
                        items={[
                { label: 'American Frontier', value: 'AmericanFrontier_SS.pdf' },
                { label: 'Donewells', value: 'Donewells_SS.pdf' },
                { label: 'Entice', value: 'Entice_SS.pdf' },
                { label: 'Ghost Tree', value: 'GhostTree.pdf' },
                { label: 'Humble Bee', value: 'HumbleBee.pdf' },
                { label: 'K10', value: 'K10_SS.pdf' },
                { label: 'Ladder 9', value: 'Ladder9.pdf' },
                { label: 'Liberty 1775', value: 'Liberty1775_SS.pdf' },
                { label: "Puckett's Branch Bourbon & Rye", value: "Puckett'sBranchBourbon&Rye.pdf" },
                { label: 'Quarter Horse Bourbon', value: 'QuarterHorseBourbon.pdf' },
                { label: 'Quarter Horse Reserve', value: 'QuarterHorseReserve.pdf' },
                { label: 'Quarter Horse Rye', value: 'QuarterHorseRye.pdf' },
                { label: 'StaveKings', value: 'StaveKings_SS.pdf' },
                { label: 'Tilted Still', value: 'TiltedStill_SS2.pdf' },
                { label: 'Western Provisions', value: 'WesternProvisions_SS.pdf' },
                { label: 'Yellow Banks', value: 'YellowBanks.pdf' }

            ]}
            onValueChange={this.onBrandSelect.bind(this)}
                        style={{
                          ...smallpickerSelectStyles,
                          iconContainer: {
                            top: 15,
                            right: 15
                          },
                          width: "100%",
                          justifyContent:"center",
                          alignContent:"center",
                          alignItem:"center",
                        
                        }}
                        
                        value={this.state.brands}
                        useNativeAndroidPickerStyle={false}
                        textInputProps={{ underlineColor: "#3B5998" }}
                        Icon={() => {
                          return (
                            <Image style={{width:20, height:10, resizeMode:"contain"}} source={require("../../assets/caret-down.png")}/>
                          );
                        }}
                      />
                      </View>
               </View>
                
               <View style={{paddingVertical:10}}></View>
                  <Button
                    bordered
                    onPress={() => {
                      this.props.navigation.navigate('showPdf',{
  file_name: 'packaging_tiers_converted.pdf'
})
                    }}
                    style={{
                      width: "100%",
                      height:80,
                      alignItems: "center",
                      justifyContent: "center",
                      borderColor: "#fff",
                      borderRadius: 5,
                      backgroundColor: "#000"
                    }}
                  >
                    <Text
                      style={{
                        textAlign: "center",
                        color: "#e8eaf6",
                        fontSize: 17,
                        fontWeight:"bold"
                      }}
                    >
                    Packaging Production Tiers
                    </Text>
                  </Button>
                  </View>
              </View>
         
          )}
        </View>
      </View>
    );
  }
}
const smallpickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 8,
    textAlign:"center",
    color: "#fff",
    width: "100%",
    // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 16,
    paddingVertical: 8,
textAlign:"center",
    color: "#fff",
    width: "100%", // to ensure the text is never behind the icon
  },
});
export default mainPage;
