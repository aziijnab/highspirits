import React, { Component } from "react";
import {
  StyleSheet,
  StatusBar,
  Text,
  Button,
  View, 
  Dimensions,
  AsyncStorage,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
  ScrollView,
} from "react-native";
import Constants from "expo-constants";

import { Body, Container, Picker } from "native-base";
import RNPickerSelect from 'react-native-picker-select';
import { MaterialIcons,Ionicons,AntDesign,Entypo  } from '@expo/vector-icons';
import { URL } from "../../components/API";
import BaseHeader from "../../components/BaseHeader/Header.js";
import { Notifications } from "expo";
import Modal from 'react-native-modal';
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

export default class SalesTeamCalculation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bottle_names_data:[],
      bottle_sizes_data:[],
      pl_type:1,
      bottle_size_id:1,
      bottle_id:1,
      packaging_tier:1,
      labor_tier:1,
      price:'',
      loading: true,
      loading_result: true,
    };
  }
  componentDidMount() {
    this.fetchBottleNames();
    this.fetchBottleSizes();
    this.fetchResults();
   this.setState({loading:false})
  }

  fetchBottleNames = () => {
    fetch(URL + "get-bottle-name", {
      method: "GET"
    })
      .then(res => res.json())
      .then(async response => {
        if (response.response == "success") {
          this.setState({
            bottle_names_data: response.data,
          });
        } else {
          Alert.alert(
            "Note",
            "Please Check Internet Connection. Thanks",
            [{ text: "OK" }],
            { cancelable: true }
          );
        }
      })
      .catch(error => alert("Please Check Your Internet Connection"));
  };
  fetchBottleSizes = () => {
    fetch(URL + "get-bottle-size", {
      method: "GET"
    })
      .then(res => res.json())
      .then(async response => {
        if (response.response == "success") {
          this.setState({
            bottle_sizes_data: response.data,
          });
        } else {
          Alert.alert(
            "Note",
            "Please Check Internet Connection. Thanks",
            [{ text: "OK" }],
            { cancelable: true }
          );
        }
      })
      .catch(error => alert("Please Check Your Internet Connection"));
  };
  fetchResults = () => {
    console.log("this.state.pl_type:")
    console.log(this.state.pl_type)
    console.log("this.state.bottle_size_id:")
    console.log(this.state.bottle_size_id)
    console.log("this.state.bottle_id:")
    console.log(this.state.bottle_id)
    console.log("this.state.packaging_tier:")
    console.log(this.state.packaging_tier)
    console.log("this.state.labor_tier:")
    console.log(this.state.labor_tier)
    this.setState({loading_result:true})
    fetch(URL + "bottle-price", {
      method: "POST",
      body: JSON.stringify({
        pl_type:this.state.pl_type,
        bottle_size_id:this.state.bottle_size_id,
        bottle_id:this.state.bottle_id,
        packaging_tier:this.state.packaging_tier,
        labor_tier:this.state.labor_tier,
      }),
      headers: {
        "Content-Type": "application/json"
      }
    })
    
      .then(res => res.json())
      .then(async response => {
        if (response.response == "success") {
          this.setState({
            price: response.data.price, loading_result:false
          });
        } else {
          Alert.alert(
            "Note",
            "Please Check Internet Connection. Thanks",
            [{ text: "OK" }],
            { cancelable: true }
          );
        }
      })
      .catch(error => alert("Please Check Your Internet Connection"));
  };
  onPLSelect(value) {

    this.setState({
      pl_type: value
    });
this.state.pl_type = value;
    this.fetchResults();
  }
  onBottleSelect(value) {
    this.setState({
      bottle_id: value
    });
    this.state.bottle_id = value;
    this.fetchResults();
  }
  onBottleSizeSelect(value) {

    this.setState({
      bottle_size_id: value
    });
    this.state.bottle_size_id = value;
    this.fetchResults();
  }
  onPackagingSelect(value) {
    this.setState({
      packaging_tier: value
    });
    this.state.packaging_tier = value;
    this.fetchResults();
  }
  onLaborSelect(value) {
    this.setState({
      labor_tier: value
    });
    this.state.labor_tier = value;
    this.fetchResults();
  }
  render() {
    const { navigation } = this.props.navigation;
   
    let distilate_arr = [];
    {this.state.bottle_names_data.map((item, index) => {
      let distilate_obj = {
        'label' : item.bottle_name,
        'value' : item.id,
      }
      distilate_arr.push(distilate_obj);
     
    })}
    let bottle_size_arr = [];
    {this.state.bottle_sizes_data.map(
      (itemSize, indexSize) => {
        let bottle_size_obj = {
          'label' : itemSize.size,
          'value' : itemSize.id
        }
        bottle_size_arr.push(bottle_size_obj);
        
      }
    )}

    return (
      <View
      style={{
          width: deviceWidth,
          height: deviceHeight,
          backgroundColor:"#003616",
          marginTop: Constants.statusBarHeight,
        }}
    >
 <View style={{ width: "100%", height: "100%" }}>
 <BaseHeader
            navigation={this.props.navigation}
            logo={true}
            back={true}
          />
          {this.state.loading ? (
            <View
              style={{
                width: deviceWidth,
                height: deviceHeight,
                alignContent: "center",
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <ActivityIndicator size={"large"} />
            </View>
          ) : (
            <ScrollView>
              <View
                style={{
                  width: "100%",
                  alignItems: "center",
                  alignContent: "center"
                }}
              >
                <View style={{ width: "90%", paddingBottom: 200 }}>
                  <Text style={{ color: "#ffffff" }}>Version 1.0 | 2020</Text>
                  <View style={{ flexDirection: "row", marginTop: 5 }}>
                    <View style={{ width: "50%", paddingRight: 10 }}>
                      <View>
                        <Text style={{ color: "#ffffff",fontSize: 20, fontWeight: "bold" }}>
                          
                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                     
                        <RNPickerSelect
                        placeholder={{}}
                        items={[
                { label: 'PL Pricing', value: 1 },
                { label: 'PL Low FOB', value: 2 }
            ]}
                        onValueChange={this.onPLSelect.bind(this)}
                        style={{
                          ...smallpickerSelectStyles,
                          iconContainer: {
                            top: 15,
                            right: 15
                          },
                          width: 30,
                          justifyContent:"center"
                        }}
                        value={this.state.lob}
                        useNativeAndroidPickerStyle={false}
                        textInputProps={{ underlineColor: "#3B5998" }}
                        Icon={() => {
                          return (
                            <Image style={{width:20, height:10, resizeMode:"contain"}} source={require("../../assets/caret-down.png")}/>
                          );
                        }}
                      />
                      </View>
                    </View>
                    <View style={{ width: "50%", paddingRight: 10 }}>
                      <View>
                        <Text style={{ color: "#ffffff",fontSize: 20, fontWeight: "bold" }}>
                          Bottle Size
                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                     <RNPickerSelect
                        placeholder={{}}
                        items={bottle_size_arr}
                        onValueChange={this.onBottleSizeSelect.bind(this)}
                        style={{
                          ...smallpickerSelectStyles,
                          iconContainer: {
                            top: 15,
                            right: 15
                          },
                          width: 30,
                          justifyContent:"center"
                        }}
                        value={this.state.distilate_id}
                        useNativeAndroidPickerStyle={false}
                        textInputProps={{ underlineColor: "#3B5998" }}
                        Icon={() => {
                          return (
                            <Image style={{width:20, height:10, resizeMode:"contain"}} source={require("../../assets/caret-down.png")}/>
                          );
                        }}
                      />
                        
                      </View>
                    </View>
                 
                       </View>
                
                
                
                  <View style={{ flexDirection: "row", marginTop: 5 }}>
                  <View style={{ width: "100%", paddingRight: 10 }}>
                      <View>
                        <Text style={{ color: "#ffffff",fontSize: 20, fontWeight: "bold" }}>
                          Liquid
                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                       
                        <RNPickerSelect
                        placeholder={{}}
                        items={distilate_arr}
                        onValueChange={this.onBottleSelect.bind(this)}
                        style={{
                          ...smallpickerSelectStyles,
                          iconContainer: {
                            top: 15,
                            right: 15
                          },
                          width: 30,
                          justifyContent:"center"
                        }}
                        value={this.state.distilate_id}
                        useNativeAndroidPickerStyle={false}
                        textInputProps={{ underlineColor: "#3B5998" }}
                        Icon={() => {
                          return (
                            <Image style={{width:20, height:10, resizeMode:"contain"}} source={require("../../assets/caret-down.png")}/>
                          );
                        }}
                      />
                      </View>
                    </View>
                  </View>
                
                
                  <View style={{ flexDirection: "row", marginTop: 5 }}>
                    <View style={{ width: "50%", paddingRight: 10 }}>
                      <View>
                        <Text style={{ color: "#ffffff",fontSize: 20, fontWeight: "bold" }}>
                          Packaging Tier
                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                     
                        <RNPickerSelect
                        placeholder={{}}
                        items={[
                { label: 'TIER I', value: 1 },
                { label: 'TIER II', value: 2 },
                { label: 'TIER III', value: 3 },
                { label: 'TIER IV', value: 4 },
            ]}
                        onValueChange={this.onPackagingSelect.bind(this)}
                        style={{
                          ...smallpickerSelectStyles,
                          iconContainer: {
                            top: 15,
                            right: 15
                          },
                          width: 30,
                          justifyContent:"center"
                        }}
                        value={this.state.lob}
                        useNativeAndroidPickerStyle={false}
                        textInputProps={{ underlineColor: "#3B5998" }}
                        Icon={() => {
                          return (
                            <Image style={{width:20, height:10, resizeMode:"contain"}} source={require("../../assets/caret-down.png")}/>
                          );
                        }}
                      />
                      </View>
                    </View>
                    <View style={{ width: "50%", paddingRight: 10 }}>
                      <View>
                        <Text style={{ color: "#ffffff",fontSize: 20, fontWeight: "bold" }}>
                          Production Tier
                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                     <RNPickerSelect
                        placeholder={{}}
                        items={[
                { label: 'TIER I', value: 1 },
                { label: 'TIER II', value: 2 },
                { label: 'TIER III', value: 3 },
                { label: 'TIER IV', value: 4 },
            ]}
                        onValueChange={this.onLaborSelect.bind(this)}
                        style={{
                          ...smallpickerSelectStyles,
                          iconContainer: {
                            top: 15,
                            right: 15
                          },
                          width: 30,
                          justifyContent:"center"
                        }}
                        value={this.state.lob}
                        useNativeAndroidPickerStyle={false}
                        textInputProps={{ underlineColor: "#3B5998" }}
                        Icon={() => {
                          return (
                            <Image style={{width:20, height:10, resizeMode:"contain"}} source={require("../../assets/caret-down.png")}/>
                          );
                        }}
                      />
                        
                      </View>
                    </View>
                 
                       </View>
                
                
                  <View style={{width:"100%", alignContent:"center", alignItems:"center"}}>
                  <View
                      style={{
                        alignContent: "center",
                        alignItems: "center",
                        marginTop:25
                      }}
                    >
                      <View>
                        <Text style={{ color:"#fff", fontSize: 20, textAlign:"center", fontWeight:"bold" }}>PRICE</Text>
                      </View>
                      <View style={{ marginTop: 10 }}>
                      {this.state.loading_result ? (
                        <ActivityIndicator size={"small"}/>
                      ) : (

                        <View style={{ padding:20, borderColor:"#fff", borderWidth:1}}>
                        <Text style={{ color: "#ffffff",fontSize: 16, fontWeight: "bold" }}>
                        $ {this.state.price}
                        </Text>
                        </View>
                      )}
                       
                      </View>
                    </View>
                  </View>
                   </View>
              </View>
            </ScrollView>
          )}
        </View>
       
     
    </View>
       
    );
  }
}
const smallpickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 8,
    paddingLeft: 7,
   
    color: "#fff",
    width: "100%"
    // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 16,
    paddingVertical: 8,
    paddingLeft: 7,
    
    color: "#fff",
    width: "100%" // to ensure the text is never behind the icon
  }
});
