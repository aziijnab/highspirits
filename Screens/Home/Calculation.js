import React, { Component } from "react";
import {
  StyleSheet,
  StatusBar,
  Text,
  Button,
  View, 
  Dimensions,
  AsyncStorage,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
  ScrollView,
} from "react-native";
import Constants from "expo-constants";

import { Container, Picker, Input } from "native-base";
import RNPickerSelect from 'react-native-picker-select';
import { MaterialIcons,Ionicons,AntDesign,Entypo  } from '@expo/vector-icons';
import { URL } from "../../components/API";
import BaseHeader from "../../components/BaseHeader/Header.js";
import { Notifications } from "expo";
import Modal from 'react-native-modal';
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class Calculation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lob: 1,
      bottled_in: 1,
      distilate_id: 3,
      terrepure_finished: 1,
      flavoured: 1,
      fet: 1,
      packaging: 2,
      bottle: 4,
      bottles_per_case: 12,
      closure: 3,
      label_config: 3,
      brands: 0,
      additional: 0,
      bottle_size: 5,
      bottling_tier_id: 4,
      packaging_tier_id: 0,
      p_tier: 2,
      total_price: 0,
      total_price_at_sales_discount: 0,
      discount_price: 0,
      selected_bottle: '',
      discount_percent: 0,
      proof: '90',
      isModalVisible: false,
      distillates_data: [],
      bottles_data: [],
      bottle_sizes_data: [],
      loading: true,
      loading_result: true,
    };
  }
  componentDidMount() {
    this.fetchData();
    this.BottlingTierCalc();
    this.PackagingTierCalc();
    this.fetchResults();
   
  }

  fetchData = () => {
    fetch(URL + "get-data", {
      method: "GET"
    })
      .then(res => res.json())
      .then(async response => {
        if (response.response == "success") {
          this.setState({
            distillates_data: response.distillates_data,
            bottles_data: response.bottles_data,
            bottle_sizes_data: response.bottle_sizes_data,

            loading: false
          });
        } else {
          Alert.alert(
            "Note",
            "Please Check Internet Connection. Thanks",
            [{ text: "OK" }],
            { cancelable: true }
          );
        }
      })
      .catch(error => alert("Please Check Your Internet Connection"));
  };
  onLobSelect(value) {
    // console.log(value);

    this.setState({
      lob: value
    });
  }
  onBottledInSelect(value) {
    // console.log(value);

    this.setState({
      bottled_in: value
    });
  }
  onDistilateSelect(value) {
    // console.log(value);

    this.setState({
      distilate_id: value
    });
    this.state.distilate_id = value;
    this.fetchResults();
  }
  setProof= (value) => {
     this.setState({proof:value})
     this.state.proof = value;
     this.fetchResults();
  }
  onTerrepureSelect(value) {
    // console.log(value);

    this.setState({
      terrepure_finished: value
    });
    this.state.terrepure_finished = value;
    this.fetchResults();
  }
  onFlavouredSelect(value) {
    // console.log(value);

    this.setState({
      flavoured: value
    });
    this.state.flavoured = value;
    this.fetchResults();
  }
  onFETSelect(value) {
    // console.log(value);

    this.setState({
      fet: value
    });
    this.state.fet = value;
    this.fetchResults();
  }
  onDiscountSelect(value) {
    // console.log(value);

    this.setState({
      discount_percent: value
    });
    this.state.discount_percent = value;
    this.fetchResults();
  }
  onBottlesPerCaseSelect(value) {
    // console.log(value);

    this.setState({
      bottles_per_case: value
    });
    this.state.bottles_per_case = value;
    this.fetchResults();
  }
  onPackagingSelect(value) {
    // console.log(value);

    this.setState({
      packaging: value
    });
    this.state.packaging = value;
    this.PackagingTierCalc();
    this.fetchResults();
  }
  
  onBottleSelect(value,bottle_name) {
    // console.log(value);
fetch(URL + "check-bottle", {
      method: "POST",
      body: JSON.stringify({
        bottle_id:value,
        size_id:this.state.bottle_size,
      }),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(async response => {
        console.log(response)
        this.toggleModal();
        if (response.response == "success") {
         if(response.is_available){
          this.setState({
            bottle: value,
            selected_bottle: bottle_name
          });
         }
         else{
           alert("Bottle not available in "+response.size+" ml")
         }
         console.log("TIER =" + response.tier)
         this.setState({p_tier : response.tier})
         this.PackagingTierCalc();
         this.fetchResults();
        } else {
          Alert.alert(
            "Note",
            "Please Check Internet Connection. Thanks",
            [{ text: "OK" }],
            { cancelable: true }
          );
        }
      })
      .catch(error => alert("Please Check Your Internet Connection"));
   
  }
  onClosureSelect(value) {
    // console.log(value);
    console.log("Bottle = "+this.state.bottle);
    if(value == 1){
      fetch(URL + "check-closure", {
        method: "POST",
        body: JSON.stringify({
          bottle_id:this.state.bottle,
        }),
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(res => res.json())
        .then(async response => {
          console.log(response)
          if (response.response == "success") {
           if(response.is_available){
            this.setState({
              closure: value
            });
            this.state.closure = value;
            this.PackagingTierCalc();
            this.BottlingTierCalc();
            this.fetchResults();
           }
           else{
             alert("Closure - Economy is not available in "+response.bottle)
           }
          } else {
            Alert.alert(
              "Note",
              "Please Check Internet Connection. Thanks",
              [{ text: "OK" }],
              { cancelable: true }
            );
          }
        })
        .catch(error => alert("Please Check Your Internet Connection"));
    }else{
      
      this.setState({
        closure: value
      });
      this.state.closure = value
      this.PackagingTierCalc();
      this.BottlingTierCalc();
      this.fetchResults();
    }
   
   
  }
  onLabelSelect(value) {
    // console.log(value);

    this.setState({
      label_config: value
    });
    this.state.label_config = value;
    this.PackagingTierCalc();
    this.BottlingTierCalc();
    this.fetchResults();
  }
  onBrandSelect(value) {
    console.log("value");
    // console.log(value);
this.props.navigation.navigate('showPdf',{
  file_name: value
})
  }
  onAdditionalSelect(value) {
    // console.log(value);

    this.setState({
      additional: value
    });
    this.state.additional = value;
    this.BottlingTierCalc();
    this.fetchResults();
  }
  onSizeSelect(value) {
    console.log("Size Id = > " + value);
    let per_case = 0;
    
    this.setState({
      bottle_size: value,
      bottles_per_case : 6
    });
    this.state.bottle_size = value;
    this.state.bottles_per_case = 6;

    this.fetchResults();
  }
 BottlingTierCalc = () => {
 
   let bottle_tier_id = 0;
   if(this.state.label_config == 1){
bottle_tier_id = 1;

   }else{
    if(this.state.closure == 3 || this.state.additional == 1){
      bottle_tier_id = 4;
     
    }else{
      if(this.state.label_config == 4){
        bottle_tier_id = 3
      }else{
        bottle_tier_id = 2
      }
    }
   }
   console.log("Bottling TIER : " + bottle_tier_id )
   this.setState({bottling_tier_id : bottle_tier_id})
   this.state.bottling_tier_id = bottle_tier_id;
 }
 PackagingTierCalc = () => {
   console.log("........................");
   console.log(this.state.packaging);
  let package_tier_id = 0;
  if(this.state.packaging == 1){
   package_tier_id = 0;
  }else{
    if(this.state.closure == 3 || this.state.label_config == 4 || this.state.label_config == 1){
      package_tier_id = 1;
    }else{
      package_tier_id = 2;
    }
    console.log("..........!= 1..package_tier_id............");
    console.log(package_tier_id);
  }
  if(package_tier_id == 0){
    this.setState({packaging_tier_id : 0 })
    this.state.packaging_tier_id = 0;
  }else if(package_tier_id == 2){
    this.setState({packaging_tier_id : 2 })
    this.state.packaging_tier_id = 2;
  }
  else{
    console.log("............p_tier............");
    console.log(this.state.p_tier);
    this.setState({packaging_tier_id : parseInt(package_tier_id)+ parseInt(this.state.p_tier) })
    this.state.packaging_tier_id = parseInt(package_tier_id)+ parseInt(this.state.p_tier);
  }
 
}
 fetchResults = () => {
   this.setState({loading_result : true})
   console.log("size_id : " + this.state.bottle_size)
   console.log("bottle_tier_id : " + this.state.bottling_tier_id)
   console.log("package_tier_id : " + this.state.packaging_tier_id)
   console.log("bottle_per_case : " + this.state.bottles_per_case)
   console.log("distilate_id : " + this.state.distilate_id)
   console.log("terrepure_finished : " + this.state.terrepure_finished)
   console.log("flavoured : " + this.state.flavoured)
   console.log("FET : " + this.state.fet)
   console.log("Proof : " + this.state.proof)
   console.log("FET FINAL : ")
   console.log(this.state.fet == 1 ? "yes" : "no")
   console.log("terrepure_finished FINAL : ")
   console.log(this.state.terrepure_finished == 1 ? "yes" : "no")
   console.log("flavoured FINAL : ")
   console.log(this.state.flavoured == 1 ? "yes" : "no")
  fetch(URL + "get-rates", {
    method: "POST",
    body: JSON.stringify({
      proof: this.state.proof,
      size_id: this.state.bottle_size,
      bottle_tier_id:this.state.bottling_tier_id,
      package_tier_id:this.state.packaging_tier_id,
      bottle_per_case:this.state.bottles_per_case,
      distilate_id:this.state.distilate_id,
      discount_percent:this.state.discount_percent,
      terrepure_finished:this.state.terrepure_finished == 1 ? "yes" : "no",
      flavoured: this.state.flavoured == 1 ? "yes" : "no",
      FET:this.state.fet == 1 ? "yes" : "no",
    }),
    headers: {
      "Content-Type": "application/json"
    }
  })
    .then(res => res.json())
    .then(async response => {
      console.log(response);
      if (response.response == "success") {
     this.setState({
       total_price : response.price,
       total_price_at_sales_discount : response.list_price_per_case_at_sales_discount,
       discount_price : response.discount,
       loading_result : false
      })
     
      } else {
        Alert.alert(
          "Note",
          "Please Check Internet Connection. Thanks",
          [{ text: "OK" }],
          { cancelable: true }
        );
      
      }
    })
    .catch(error => alert("Please Check Your Internet Connection"));
 }
 toggleModal = () => {
  this.setState({isModalVisible : !this.state.isModalVisible});
};
 show_bottles_modal = () => {
   this.toggleModal();
   console.log("Bottles Picker"); 
  }
  render() {
    const { navigation } = this.props.navigation;
    const placeholder_brands = {
      label: "Brands...",
      value: null,
      color: "#eaeaea",
      key: 0
    };
    let distilate_arr = [];
    {this.state.distillates_data.map((item, index) => {
      let distilate_obj = {
        'label' : item.distilate_name,
        'value' : item.id,
      }
      distilate_arr.push(distilate_obj);
     
    })}
    let bottle_size_arr = [];
    {this.state.bottle_sizes_data.map(
      (itemSize, indexSize) => {
        let bottle_size_obj = {
          'label' : itemSize.size+"ml",
          'value' : itemSize.id
        }
        bottle_size_arr.push(bottle_size_obj);
        
      }
    )}

    return (
      <View
     style={{
          width: deviceWidth,
          height: deviceHeight,
          backgroundColor:"#003616",
          marginTop: Constants.statusBarHeight,
        }}
    >
 <View style={{ width: "100%", height: "100%" }}>
 <BaseHeader
            navigation={this.props.navigation}
            logo={true}
            back={true}
          />
          {this.state.loading ? (
            <View
              style={{
                width: deviceWidth,
                height: deviceHeight,
                alignContent: "center",
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <ActivityIndicator size={"large"} />
            </View>
          ) : (
            <ScrollView>
              <View
                style={{
                  width: "100%",
                  alignItems: "center",
                  alignContent: "center"
                }}
              >
                <View style={{ width: "90%", paddingBottom: 200 }}>
                  <Text style={{ color: "#ffffff" }}>Version 1.0 | 2020</Text>
                  <View style={{ flexDirection: "row", marginTop: 5 }}>
                    <View style={{ width: "50%", paddingRight: 10 }}>
                      <View>
                        <Text style={{ color: "#ffffff",fontSize: 20, fontWeight: "bold" }}>
                          LOB
                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                        {/* <Picker
                          mode="dropdown"
                          style={{ width: undefined, height: 40, color:"#fff", }}
                          placeholder="LOB"
                          placeholderStyle={{ color: "#eaeaea" }}
                          placeholderIconColor={"#fff"}
                          selectedValue={this.state.lob}
                          onValueChange={this.onLobSelect.bind(this)}
                        >
                          <Picker.Item label="Private Brand" value={1} />
                          <Picker.Item label="Private Label" value={2} />
                        </Picker> */}
                        <RNPickerSelect
                        placeholder={{}}
                        items={[
                { label: 'Private Brand', value: 1 },
                { label: 'Private Label', value: 2 }
            ]}
                        onValueChange={this.onLobSelect.bind(this)}
                        style={{
                          ...smallpickerSelectStyles,
                          iconContainer: {
                            top: 15,
                            right: 15
                          },
                          width: 30,
                          justifyContent:"center"
                        }}
                        value={this.state.lob}
                        useNativeAndroidPickerStyle={false}
                        textInputProps={{ underlineColor: "#3B5998" }}
                        Icon={() => {
                          return (
                            <Image style={{width:20, height:10, resizeMode:"contain"}} source={require("../../assets/caret-down.png")}/>
                          );
                        }}
                      />
                      </View>
                    </View>
                    <View style={{ width: "50%", paddingRight: 10 }}>
                      <View>
                        <Text style={{ color: "#ffffff",fontSize: 20, fontWeight: "bold" }}>
                          Distillate
                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                       
                        <RNPickerSelect
                        placeholder={{}}
                        items={distilate_arr}
                        onValueChange={this.onDistilateSelect.bind(this)}
                        style={{
                          ...smallpickerSelectStyles,
                          iconContainer: {
                            top: 15,
                            right: 15
                          },
                          width: 30,
                          justifyContent:"center"
                        }}
                        value={this.state.distilate_id}
                        useNativeAndroidPickerStyle={false}
                        textInputProps={{ underlineColor: "#3B5998" }}
                        Icon={() => {
                          return (
                            <Image style={{width:20, height:10, resizeMode:"contain"}} source={require("../../assets/caret-down.png")}/>
                          );
                        }}
                      />
                      </View>
                    </View>
                  </View>
                
                  <View
                    style={{ flexDirection:"row", paddingRight: 10, marginTop: 5, justifyContent:"space-between" }}
                  >
                  <View style={{width:"60%"}}>
                  <View>
                      <Text style={{ color: "#ffffff",fontSize: 20, fontWeight: "bold" }}>
                      TerrePURE ® Finished
                      </Text>
                    </View>
                    <View
                      style={{
                        width: "100%",
                        borderWidth: 1.2,
                        borderRadius: 10,
                        borderColor: "#ffffff",
                        padding: 0,
                        marginTop: 10
                      }}
                    >
                      
                      <RNPickerSelect
                        placeholder={{}}
                        items={[
                { label: 'Yes', value: 1 },
                { label: 'No', value: 2 }
            ]}
            onValueChange={this.onTerrepureSelect.bind(this)}
                        style={{
                          ...smallpickerSelectStyles,
                          iconContainer: {
                            top: 15,
                            right: 15
                          },
                          width: 30,
                          justifyContent:"center"
                        }}
                        value={this.state.terrepure_finished}
                        useNativeAndroidPickerStyle={false}
                        textInputProps={{ underlineColor: "#3B5998" }}
                        Icon={() => {
                          return (
                            <Image style={{width:20, height:10, resizeMode:"contain"}} source={require("../../assets/caret-down.png")}/>
                          );
                        }}
                      />
                    </View>
                 
                  </View>
                    
                    <View style={{width:"35%"}}>
                    <View>
                        <Text style={{ color: "#ffffff",fontSize: 20, fontWeight: "bold" }}>
                          Proof
                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                          <Input 
                          style={{color:"#fff", textAlign:"center"}} 
                          onChangeText={(proof)=> this.setProof(proof)} 
                          keyboardType={"numeric"}
                          value={this.state.proof}/>
                        
                      </View>
                   
                    </View>
                    </View>

                  {/* ..........ROW 2................ */}
                  <View style={{ flexDirection: "row", marginTop: 5 }}>
                    <View style={{ width: "33%", paddingRight: 10 }}>
                      <View>
                        <Text style={{ color: "#ffffff",fontSize: 20, fontWeight: "bold" }}>
                        Flavored
                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                        {/* <Picker
                          mode="dropdown"
                          style={{ width: undefined, height: 40, color:"#fff" }}
                          placeholder="Flavored"
                          placeholderStyle={{ color: "#eaeaea" }}
                          placeholderIconColor="#eaeaea"
                          selectedValue={this.state.flavoured}
                          onValueChange={this.onFlavouredSelect.bind(this)}
                        >
                          <Picker.Item label="Yes" value={1} />
                          <Picker.Item label="No" value={0} />
                        </Picker> */}
                        <RNPickerSelect
                        placeholder={{}}
                        items={[
                { label: 'Yes', value: 1 },
                { label: 'No', value: 2 }
            ]}
            onValueChange={this.onFlavouredSelect.bind(this)}
                        style={{
                          ...smallpickerSelectStyles,
                          iconContainer: {
                            top: 15,
                            right: 15
                          },
                          width: 30,
                          justifyContent:"center"
                        }}
                        value={this.state.flavoured}
                        useNativeAndroidPickerStyle={false}
                        textInputProps={{ underlineColor: "#3B5998" }}
                        Icon={() => {
                          return (
                            <Image style={{width:20, height:10, resizeMode:"contain"}} source={require("../../assets/caret-down.png")}/>
                          );
                        }}
                      />
                      </View>
                    </View>
                    <View style={{ width: "33%", paddingRight: 10 }}>
                      <View>
                        <Text style={{ color: "#ffffff",fontSize: 20, fontWeight: "bold" }}>
                          Bottled In
                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                        {/* <Picker
                          mode="dropdown"
                          style={{ width: undefined, height: 40, color:"#fff" }}
                          placeholder="Bottled In"
                          placeholderStyle={{ color: "#eaeaea" }}
                          placeholderIconColor="#eaeaea"
                          selectedValue={this.state.bottled_in}
                          onValueChange={this.onBottledInSelect.bind(this)}
                        >
                          <Picker.Item label="KY" value={1} />
                          <Picker.Item label="SC" value={2} />
                        </Picker> */}
                        <RNPickerSelect
                        placeholder={{}}
                        items={[
                { label: 'KY', value: 1 },
                { label: 'SC', value: 2 }
            ]}
            onValueChange={this.onBottledInSelect.bind(this)}
                        style={{
                          ...smallpickerSelectStyles,
                          iconContainer: {
                            top: 15,
                            right: 15
                          },
                          width: 30,
                          justifyContent:"center"
                        }}
                        value={this.state.bottled_in}
                        useNativeAndroidPickerStyle={false}
                        textInputProps={{ underlineColor: "#3B5998" }}
                        Icon={() => {
                          return (
                            <Image style={{width:20, height:10, resizeMode:"contain"}} source={require("../../assets/caret-down.png")}/>
                          );
                        }}
                      />
                      </View>
                    </View>
                    <View style={{ width: "33%", paddingRight: 10 }}>
                      <View>
                        <Text style={{ color: "#ffffff",fontSize: 20, fontWeight: "bold" }}>
                          FET
                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                        {/* <Picker
                          mode="dropdown"
                          style={{ width: undefined, height: 40, color:"#fff" }}
                          placeholder="FET"
                          placeholderStyle={{ color: "#eaeaea" }}
                          placeholderIconColor="#eaeaea"
                          selectedValue={this.state.fet}
                          onValueChange={this.onFETSelect.bind(this)}
                        >
                          <Picker.Item label="Yes" value={1} />
                          <Picker.Item label="No" value={0} />
                        </Picker> */}
                        <RNPickerSelect
                        placeholder={{}}
                        items={[
                { label: 'Yes', value: 1 },
                { label: 'No', value: 2 }
            ]}
            onValueChange={this.onFETSelect.bind(this)}
                        style={{
                          ...smallpickerSelectStyles,
                          iconContainer: {
                            top: 15,
                            right: 15
                          },
                          width: 30,
                          justifyContent:"center"
                        }}
                        value={this.state.fet}
                        useNativeAndroidPickerStyle={false}
                        textInputProps={{ underlineColor: "#3B5998" }}
                        Icon={() => {
                          return (
                            <Image style={{width:20, height:10, resizeMode:"contain"}} source={require("../../assets/caret-down.png")}/>
                          );
                        }}
                      />

                      </View>
                    </View>
                  </View>
                  <View style={{ flexDirection: "row", marginTop: 5 }}>
                    <View style={{ width: "50%", paddingRight: 10 }}>
                      <View>
                        <Text style={{ color: "#ffffff",fontSize: 20, fontWeight: "bold" }}>
                          Bottle Size
                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                        
                        <RNPickerSelect
                        placeholder={{}}
                        items={bottle_size_arr}
                        onValueChange={this.onSizeSelect.bind(this)}
                        style={{
                          ...smallpickerSelectStyles,
                          iconContainer: {
                            top: 15,
                            right: 15
                          },
                          width: 30,
                          justifyContent:"center"
                        }}
                        value={this.state.bottle_size}
                        useNativeAndroidPickerStyle={false}
                        textInputProps={{ underlineColor: "#3B5998" }}
                        Icon={() => {
                          return (
                            <Image style={{width:20, height:10, resizeMode:"contain"}} source={require("../../assets/caret-down.png")}/>
                          );
                        }}
                      />
                      </View>
                    </View>

                    <View style={{ width: "50%", paddingRight: 10 }}>
                      <View>
                        <Text style={{ color: "#ffffff",fontSize: 20, fontWeight: "bold" }}>
                          Bottles Per Case
                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                       {this.state.bottle_size == 1 ? (
                        
                        <RNPickerSelect
                        placeholder={{}}
                        items={[
                { label: '60', value: 60 },
                { label: '120', value: 120 }
            ]}
            onValueChange={this.onBottlesPerCaseSelect.bind(this)}
                        style={{
                          ...smallpickerSelectStyles,
                          iconContainer: {
                            top: 15,
                            right: 15
                          },
                          width: 30,
                          justifyContent:"center"
                        }}
                        value={this.state.bottles_per_case}
                        useNativeAndroidPickerStyle={false}
                        textInputProps={{ underlineColor: "#3B5998" }}
                        Icon={() => {
                          return (
                            <Image style={{width:20, height:10, resizeMode:"contain"}} source={require("../../assets/caret-down.png")}/>
                          );
                        }}
                      />

                        ) : this.state.bottle_size == 5 ? (
                         
                        <RNPickerSelect
                        placeholder={{}}
                        items={[
                { label: '6', value: 6 },
                { label: '12', value: 12 }
            ]}
            onValueChange={this.onBottlesPerCaseSelect.bind(this)}
                        style={{
                          ...smallpickerSelectStyles,
                          iconContainer: {
                            top: 15,
                            right: 15
                          },
                          width: 30,
                          justifyContent:"center"
                        }}
                        value={this.state.bottles_per_case}
                        useNativeAndroidPickerStyle={false}
                        textInputProps={{ underlineColor: "#3B5998" }}
                        Icon={() => {
                          return (
                            <Image style={{width:20, height:10, resizeMode:"contain"}} source={require("../../assets/caret-down.png")}/>
                          );
                        }}
                      />
                        ) : this.state.bottle_size == 6 ? (
                          
                        <RNPickerSelect
                        placeholder={{}}
                        items={[
                { label: '6', value: 6 },
                { label: '9', value: 9 },
                { label: '12', value: 12 }
            ]}
            onValueChange={this.onBottlesPerCaseSelect.bind(this)}
                        style={{
                          ...smallpickerSelectStyles,
                          iconContainer: {
                            top: 15,
                            right: 15
                          },
                          width: 30,
                          justifyContent:"center"
                        }}
                        value={this.state.bottles_per_case}
                        useNativeAndroidPickerStyle={false}
                        textInputProps={{ underlineColor: "#3B5998" }}
                        Icon={() => {
                          return (
                            <Image style={{width:20, height:10, resizeMode:"contain"}} source={require("../../assets/caret-down.png")}/>
                          );
                        }}
                      />
                        ) : (
                        
                        <RNPickerSelect
                        placeholder={{}}
                        items={[
                { label: '6', value: 6 },
            ]}
            onValueChange={this.onBottlesPerCaseSelect.bind(this)}
                        style={{
                          ...smallpickerSelectStyles,
                          iconContainer: {
                            top: 15,
                            right: 15
                          },
                          width: 30,
                          justifyContent:"center"
                        }}
                        value={this.state.bottles_per_case}
                        useNativeAndroidPickerStyle={false}
                        textInputProps={{ underlineColor: "#3B5998" }}
                        Icon={() => {
                          return (
                            <Image style={{width:20, height:10, resizeMode:"contain"}} source={require("../../assets/caret-down.png")}/>
                          );
                        }}
                      />
                        )}
                        
                      </View>
                    </View>
                  </View>

                  <View
                    style={{ width: "100%", paddingRight: 10, marginTop: 5 }}
                  >
                    <View>
                      <Text style={{ color: "#ffffff",fontSize: 20, fontWeight: "bold" }}>
                        Packaging
                      </Text>
                    </View>
                    <View
                      style={{
                        width: "100%",
                        borderWidth: 1.2,
                        borderRadius: 10,
                        borderColor: "#ffffff",
                        padding: 0,
                        marginTop: 10
                      }}
                    >
                      {/* <Picker
                        mode="dropdown"
                        style={{ width: undefined, height: 40, color:"#fff" }}
                        placeholder="Packaging"
                        placeholderStyle={{ color: "#eaeaea" }}
                        placeholderIconColor="#eaeaea"
                        selectedValue={this.state.packaging}
                        onValueChange={this.onPackagingSelect.bind(this)}
                      >
                        <Picker.Item label="Customer Provided" value={1} />
                        <Picker.Item label="Green River Sourced" value={2} />
                      </Picker> */}
                      <RNPickerSelect
                        placeholder={{}}
                        items={[
                { label: 'Customer Provided', value: 1 },
                { label: 'Green River Sourced', value: 2 }
            ]}
            onValueChange={this.onPackagingSelect.bind(this)}
                        style={{
                          ...smallpickerSelectStyles,
                          iconContainer: {
                            top: 15,
                            right: 15
                          },
                          width: 30,
                          justifyContent:"center"
                        }}
                        value={this.state.packaging}
                        useNativeAndroidPickerStyle={false}
                        textInputProps={{ underlineColor: "#3B5998" }}
                        Icon={() => {
                          return (
                            <Image style={{width:20, height:10, resizeMode:"contain"}} source={require("../../assets/caret-down.png")}/>
                          );
                        }}
                      />
                    </View>
                  </View>

                  <View style={{ flexDirection: "row", marginTop: 5 }}>
                    <View style={{ width: "50%", paddingRight: 10 }}>
                      <View>
                        <Text style={{ color: "#ffffff",fontSize: 20, fontWeight: "bold" }}>
                          Bottle
                        </Text>
                      </View>
                      {this.state.packaging_tier_id == 0 ? (
                        <View
                        style={{
                          width: "100%",
                          height:40,
                          backgroundColor:"#eaeaea",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#eaeaea",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                       
                      </View>
                 
                      ):(
                        <TouchableOpacity
                        onPress={this.show_bottles_modal}
                        style={{
                          width: "100%",
                          height: 40,
                          alignItems:"center",
                          alignContent:"center",
                          justifyContent:"center",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                      {this.state.selected_bottle ? (
                      <Text style={{fontSize:18, color:"#fff"}}>{this.state.selected_bottle}</Text>

                      ) : (

                      <Text style={{fontSize:18, color:"#fff"}}>Select</Text>
                      )}
                         </TouchableOpacity>
                 
                      )}
                        </View>
                    <View style={{ width: "50%", paddingRight: 10 }}>
                      <View>
                        <Text style={{ color: "#ffffff",fontSize: 20, fontWeight: "bold" }}>
                          Closure
                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                        {/* <Picker
                          mode="dropdown"
                          style={{ width: undefined, height: 40, color:"#fff" }}
                          placeholder="Closure"
                          placeholderStyle={{ color: "#eaeaea" }}
                          placeholderIconColor="#eaeaea"
                          selectedValue={this.state.closure}
                          onValueChange={this.onClosureSelect.bind(this)}
                        >
                          <Picker.Item label="Economy" value={1} />
                          <Picker.Item label="Standard" value={2} />
                          <Picker.Item label="Designer" value={3} />
                        </Picker> */}
                        <RNPickerSelect
                        placeholder={{}}
                        items={[
                { label: 'Economy', value: 1 },
                { label: 'Standard', value: 2 },
                { label: 'Designer', value: 3 },
            ]}
            onValueChange={this.onClosureSelect.bind(this)}
                        style={{
                          ...smallpickerSelectStyles,
                          iconContainer: {
                            top: 15,
                            right: 15
                          },
                          width: 30,
                          justifyContent:"center"
                        }}
                        value={this.state.closure}
                        useNativeAndroidPickerStyle={false}
                        textInputProps={{ underlineColor: "#3B5998" }}
                        Icon={() => {
                          return (
                            <Image style={{width:20, height:10, resizeMode:"contain"}} source={require("../../assets/caret-down.png")}/>
                          );
                        }}
                      />
                      </View>
                    </View>
                  </View>
                  <View style={{ flexDirection: "row", marginTop: 5 }}>
                    <View style={{ width: "50%", paddingRight: 10 }}>
                      <View>
                        <Text style={{ color: "#ffffff",fontSize: 20, fontWeight: "bold" }}>
                          Label Config
                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                        {/* <Picker
                          mode="dropdown"
                          style={{ width: undefined, height: 40, color:"#fff" }}
                          placeholder="Label Config"
                          placeholderStyle={{ color: "#eaeaea" }}
                          placeholderIconColor="#eaeaea"
                          selectedValue={this.state.label_config}
                          onValueChange={this.onLabelSelect.bind(this)}
                        >
                          <Picker.Item label="Screen Printed" value={1} />
                          <Picker.Item label="Economy" value={2} />
                          <Picker.Item label="Standard" value={3} />
                          <Picker.Item label="Designer" value={4} />
                        </Picker> */}
                        <RNPickerSelect
                        placeholder={{}}
                        items={[
                { label: 'Screen Printed', value: 1 },
                { label: 'Economy', value: 2 },
                { label: 'Standard', value: 3 },
                { label: 'Designer', value: 4 },
            ]}
            onValueChange={this.onLabelSelect.bind(this)}
                        style={{
                          ...smallpickerSelectStyles,
                          iconContainer: {
                            top: 15,
                            right: 15
                          },
                          width: 30,
                          justifyContent:"center"
                        }}
                        value={this.state.label_config}
                        useNativeAndroidPickerStyle={false}
                        textInputProps={{ underlineColor: "#3B5998" }}
                        Icon={() => {
                          return (
                            <Image style={{width:20, height:10, resizeMode:"contain"}} source={require("../../assets/caret-down.png")}/>
                          );
                        }}
                      />
                      </View>
                    </View>
                    {/* <View style={{ width: "50%", paddingRight: 10 }}>
                      <View>
                        <Text style={{ color: "#ffffff",fontSize: 20, fontWeight: "bold" }}>
                          Additional
                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                        <Picker
                          mode="dropdown"
                          style={{ width: undefined, height: 40, color:"#fff" }}
                          placeholder="Additional"
                          placeholderStyle={{ color: "#eaeaea" }}
                          placeholderIconColor="#eaeaea"
                          selectedValue={this.state.additional}
                          onValueChange={this.onAdditionalSelect.bind(this)}
                        >
                          <Picker.Item label="Yes" value={1} />
                          <Picker.Item label="No" value={0} />
                        </Picker>
                      </View>
                    </View> */}
                  </View>

                  <View
                    style={{
                      width: "100%",
                      borderColor: "#eaeaea",
                      borderTopWidth: 0.8,
                      marginTop: 20
                    }}
                  ></View>

                  <View style={{ flexDirection: "row", marginTop: 20 }}>
                    <View
                      style={{
                        width: "50%",
                        alignContent: "center",
                        alignItems: "center"
                      }}
                    >
                      <View>
                        <Text style={{ color:"#fff", fontSize: 20 }}>Packaging Tier</Text>
                      </View>
                      <View>
                      {this.state.packaging_tier_id == 1 ? (
                        <Text style={{ color: "#ffffff",fontSize: 20, fontWeight: "bold" }}>
                          TIER I
                        </Text>

                      ) : this.state.packaging_tier_id == 2 ? (
                        <Text style={{ color: "#ffffff",fontSize: 20, fontWeight: "bold" }}>
                          TIER II
                        </Text>

                      ) : this.state.packaging_tier_id == 3 ? (
                        <Text style={{ color: "#ffffff",fontSize: 20, fontWeight: "bold" }}>
                          TIER III
                        </Text>

                      ) : this.state.packaging_tier_id == 4 ? (
                        <Text style={{ color: "#ffffff",fontSize: 20, fontWeight: "bold" }}>
                          TIER IV
                        </Text>

                      ) : this.state.packaging_tier_id == 5 ? (
                        <Text style={{ color: "#ffffff",fontSize: 20, fontWeight: "bold" }}>
                          TIER V
                        </Text>

                      ) : (
                        <Text style={{ color: "#ffffff",fontSize: 20, fontWeight: "bold" }}>
                          N/A
                        </Text>

                      )}
                      </View>
                    </View>
                    <View
                      style={{
                        width: "50%",
                        alignContent: "center",
                        alignItems: "center"
                      }}
                    >
                      <View>
                        <Text style={{ color:"#fff", fontSize: 20 }}>Production Tier</Text>
                      </View>
                      <View>
                      {this.state.bottling_tier_id == 1 ? (
                        <Text style={{ color: "#ffffff",fontSize: 20, fontWeight: "bold" }}>
                          TIER I
                        </Text>

                      ) : this.state.bottling_tier_id == 2 ? (
                        <Text style={{ color: "#ffffff",fontSize: 20, fontWeight: "bold" }}>
                          TIER II
                        </Text>

                      ) : this.state.bottling_tier_id == 3 ? (
                        <Text style={{ color: "#ffffff",fontSize: 20, fontWeight: "bold" }}>
                          TIER III
                        </Text>

                      ) :(
                        <Text style={{ color: "#ffffff",fontSize: 20, fontWeight: "bold" }}>
                          TIER IV
                        </Text>

                      )}
                      </View>
                    </View>
                    <View style={{ width: "50%" }}></View>
                  </View>

                  <View style={{ width:"100%", marginTop: 20, alignContent:"center", alignItems:"center" }}>
                    <View
                      style={{
                        width: "50%",
                        alignContent: "center",
                        alignItems: "center"
                      }}
                    >
                      <View>
                        <Text style={{ color:"#fff", fontSize: 20, textAlign:"center" }}>List Price Per Case</Text>
                      </View>
                      <View style={{ marginTop: 10 }}>
                      {this.state.loading_result ? (
                        <ActivityIndicator size={"small"}/>
                      ) : (
                        <Text style={{ color: "#ffffff",fontSize: 20, fontWeight: "bold" }}>
                          $ {parseFloat(this.state.total_price).toFixed(2) }
                        </Text>
                      )}
                        
                      </View>
                    </View>
                  </View>
                   {/* <View style={{width:"100%", marginTop:20, alignContent:"center", alignItems:"center"}}>
                    <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                     
                        <RNPickerSelect
                        placeholder={placeholder_brands}
                        items={[
                { label: 'American Frontier', value: 'AmericanFrontier_SS.pdf' },
                { label: 'Donewells', value: 'Donewells_SS.pdf' },
                { label: 'Entice', value: 'Entice_SS.pdf' },
                { label: 'Ghost Tree', value: 'GhostTree.pdf' },
                { label: 'Humble Bee', value: 'HumbleBee.pdf' },
                { label: 'K10', value: 'K10_SS.pdf' },
                { label: 'Ladder 9', value: 'Ladder9.pdf' },
                { label: 'Liberty 1775', value: 'Liberty1775_SS.pdf' },
                { label: "Puckett's Branch Bourbon & Rye", value: "Puckett'sBranchBourbon&Rye.pdf" },
                { label: 'Quarter Horse Bourbon', value: 'QuarterHorseBourbon.pdf' },
                { label: 'Quarter Horse Reserve', value: 'QuarterHorseReserve.pdf' },
                { label: 'Quarter Horse Rye', value: 'QuarterHorseRye.pdf' },
                { label: 'StaveKings', value: 'StaveKings_SS.pdf' },
                { label: 'Tilted Still', value: 'TiltedStill_SS2.pdf' },
                { label: 'Western Provisions', value: 'WesternProvisions_SS.pdf' },
                { label: 'Yellow Banks', value: 'YellowBanks.pdf' }

            ]}
            onValueChange={this.onBrandSelect.bind(this)}
                        style={{
                          ...smallpickerSelectStyles,
                          iconContainer: {
                            top: 15,
                            right: 15
                          },
                          width: 30,
                          justifyContent:"center"
                        }}
                        value={this.state.brands}
                        useNativeAndroidPickerStyle={false}
                        textInputProps={{ underlineColor: "#3B5998" }}
                        Icon={() => {
                          return (
                            <Image style={{width:20, height:10, resizeMode:"contain"}} source={require("../../assets/caret-down.png")}/>
                          );
                        }}
                      />
                      </View>
               </View> */}
                </View>
              </View>
            </ScrollView>
          )}
        </View>
        <Modal isVisible={this.state.isModalVisible}>
          <View style={{width: "100%", height:"100%", backgroundColor:"#fff"}}>
<ScrollView style={{width: "100%"}}>
<View style={{flexDirection:"row", flexWrap: "wrap"}}>
{this.state.bottles_data.map((item, index) => {
  if(item.bottle_image){

  return(
    <TouchableOpacity onPress={()=> this.onBottleSelect(item.id,item.bottle_name)} key={index} style={{width:"50%", alignContent:"center", alignItems:"center", paddingVertical:20, borderWidth:0.8, borderColor:"#fff"}}>
<Image style={{width:100, height:100, resizeMode:"cover"}} source={{uri:"http://winery.brewerydtc.com/public/"+item.bottle_image}}/>
           </TouchableOpacity>
  )
  }
})
}
          
</View>
           
</ScrollView>
             

            <Button title="Hide modal" onPress={this.toggleModal} />
          </View>
        </Modal>
     
    </View>
       
    );
  }
}
const smallpickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 8,
    paddingLeft: 7,
   
    color: "#fff",
    width: "100%"
    // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 16,
    paddingVertical: 8,
    paddingLeft: 7,
    
    color: "#fff",
    width: "100%" // to ensure the text is never behind the icon
  }
});
export default Calculation;
