import React, { Component } from "react";
import {
  StyleSheet,
  StatusBar,
  Text,
  View, 
  Dimensions,
  AsyncStorage,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
  ScrollView,
} from "react-native";
import Constants from "expo-constants";

import { Container, Picker, Input, Button } from "native-base";
import RNPickerSelect from 'react-native-picker-select';
import { Ionicons } from "@expo/vector-icons";
import { URL } from "../../components/API";
import BaseHeader from "../../components/BaseHeader/Header.js";
import { Notifications } from "expo";
import Modal from 'react-native-modal';
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class UnitEconomics extends Component {
  constructor(props) {
    super(props);
    this.state = {
      calculating : false,
      showData : "Ladder #9",
      brand : "",
      type : "",
      bottle_size : "",
      abv : "",
      retail_price_p_bottle : "",
      case_size : "",
      no_of_pallet : "",
      case_per_pallet : "",
      retail_margin : "",
      distributor_margin : "",
      domestic_transportation : "",
      ap_support : "",
      pbc : "",
      retail_margin_bottle : "",
    };
  }
  componentDidMount() {
 this.fetch_unit_rates();   
  }
  onBrandSelect(value) {
  
        this.setState({showData:value})
        this.state.showData = value;
        this.fetch_unit_rates();
  }
   onTypeSelect(value) {

this.setState({type:value})
  }
  onBottleSizeSelect(value) {

this.setState({bottle_size:value})
  }
  fetch_unit_rates = () => {
    let brand = this.state.showData;
    fetch(URL + "get-unit-rates", {
      method: "POST",
      body: JSON.stringify({
        brand:brand
       }),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(async response => {
        if (response.response == "success") {
          console.log("response");
          console.log(response);
          this.setState({
            type : response.data.type,
            bottle_size : response.data.bottle_size,
            abv : response.data.abv  ,
            retail_price_p_bottle : response.data.retail_price  ,
            case_size : response.data.case_size  ,
            no_of_pallet : response.data.no_of_pallets  ,
            case_per_pallet : response.data.case_per_pallet  ,
            retail_margin : response.data.retail_margin  ,
            distributor_margin : response.data.distributor_margin  ,
            domestic_transportation : response.data.domestic_transportation  ,
            ap_support : response.data.ap_support  ,
            pbc : response.data.pbc  ,
            retail_margin_bottle : response.data.retail_margin_bottle  ,
          });
        } else {
          Alert.alert(
            "Note",
            "Please Check Internet Connection. Thanks",
            [{ text: "OK" }],
            { cancelable: true }
          );
        }
      })
      .catch(error => alert("Please Check Your Internet Connection"));

  }
  calculate = () => {
    this.setState({calculating:true})
   
    let retail_margin_per_bottle = this.state.retail_margin_bottle;
    console.log("State");
    console.log(this.state);
    console.log("--------------");
    fetch(URL + "get-calculations", {
      method: "POST",
      body: JSON.stringify({
        brand:this.state.showData,
        type:this.state.type,
        abv:this.state.abv,
        bottle_size:this.state.bottle_size,
        retail_price:this.state.retail_price_p_bottle,
        case_size:this.state.case_size,
        no_of_pallets:this.state.no_of_pallet,
        case_per_pallet:this.state.case_per_pallet,
        retail_margin:this.state.retail_margin,
        distributor_margin:this.state.distributor_margin,
        domestic_transportation:this.state.domestic_transportation,
        ap_support:this.state.ap_support,
        pbc:this.state.pbc,
        retail_margin_per_bottle:this.state.retail_margin_bottle,
       }),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(async response => {
        this.setState({calculating:false})
        if (response.response == "success") {
          console.log("response");
          console.log(response);
           this.props.navigation.navigate("table",{
            calculations: response.data,
            brand: this.state.showData
          });
        } else {
          Alert.alert(
            "Note",
            "Please Check Internet Connection. Thanks",
            [{ text: "OK" }],
            { cancelable: true }
          );
        }
      })
      .catch(error => alert("Please Check Your Internet Connection"));
  }
 
  render() {
    const { navigation } = this.props.navigation;
    let isAPdisabled = false;
    if(this.state.showData == "Yellow Banks w necker"){
      isAPdisabled = true;
    }
    return (
      <View
      style={{
          width: deviceWidth,
          height: deviceHeight,
          backgroundColor:"#003616",
          marginTop: Constants.statusBarHeight,
        }}
    >
 <View style={{ width: "100%", height: "100%" }}>
 <BaseHeader
            navigation={this.props.navigation}
            logo={true}
            back={true}
          />
          {this.state.loading ? (
            <View
              style={{
                width: deviceWidth,
                height: deviceHeight,
                alignContent: "center",
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <ActivityIndicator size={"large"} />
            </View>
          ) : (
            <ScrollView>
              <View
                style={{
                  width: "100%",
                  alignItems: "center",
                  alignContent: "center"
                }}
              >
                <View style={{ width: "90%", paddingBottom: 200 }}>
                  <Text style={{ color: "#ffffff" }}>Version 1.0 | 2020</Text>
                  <View style={{ width:"100%", marginTop: 5, alignContent:"center", alignItems:"center" }}>
                    <View style={{ width: "95%", paddingRight: 10 }}>
                      <View>
                        <Text style={{ color: "#ffffff",fontSize: 16, fontWeight: "bold" }}>
                          Select Brand
                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                        
                        <RNPickerSelect
                        placeholder={{}}
                        items={[
                { label: 'Select', value: 0 },
                { label: 'PL-PB Brand', value: 'PL-PB Brand' },
                { label: 'American Frontier Vodka', value: 'American Frontier Vodka' },
                { label: "Donewell's Flavored Whiskies", value: "Donewell's Flavored Whiskies" },
                { label: "Donewell's Straight Bourbon", value: "Donewell's Straight Bourbon" },
                { label: "Donewell's Straight Rye", value: "Donewell's Straight Rye" },
                { label: "Entice Vodka", value: "Entice Vodka" },
                { label: "Hixson Gin", value: "Hixson Gin" },
                { label: "Hixson Rose Gin", value: "Hixson Rose Gin" },
                { label: "Humble Bee", value: 'Humble Bee' },
                { label: "KY 10", value: "KY 10" },
                { label: 'Ladder #9', value: 'Ladder #9' },
                { label: "Liberty 1775", value: "Liberty 1775" },
                { label: "Stave Kings", value: "Stave Kings" },
                { label: "Western Provisions", value: "Western Provisions" },
                { label: "Tilted Still 100 pf Moonshine", value: "Tilted Still 100 pf Moonshine" },
                { label: "Tilted Still 70 pf Moonshine", value: "Tilted Still 70 pf Moonshine" },
                { label: "Tilted Still 40 pf Moonshine", value: "Tilted Still 40 pf Moonshine" },
                { label: "Ghost Tree Moonshine", value: "Ghost Tree Moonshine" },
                { label: "Pucket's Branch Bourbon", value: "Pucket's Branch Bourbon" },
                { label: "Pucket's Branch Rye", value: "Pucket's Branch Rye" },
                // { label: 'Noteworthy', value: 'Noteworthy ARE' },
                // { label: 'Yellow Banks w necker', value: 'Yellow Banks w necker' },
                // { label: 'Press & Clover', value: 'Press & Clover' },
                // { label: 'Tilted Still Flavors', value: 'Tilted Still Flavors' },
                // { label: 'Tilted Still Original', value: 'Tilted Still Original' },
                // { label: 'Hixson Gin 1.75', value: 'Hixson Gin 1.75' },
                // { label: 'Hixson Gin 0.750', value: 'Hixson Gin 0.750' },
                // { label: '10 Distillery Rd w Necker', value: '10 Distillery Rd w Necker' },
                // { label: 'Noteworthy PBC 1.75', value: 'Noteworthy PBC 1.75' },
            ]}
                        onValueChange={this.onBrandSelect.bind(this)}
                        style={{
                          ...smallpickerSelectStyles,
                          iconContainer: {
                            top: 15,
                            right: 15
                          },
                          width: 30,
                          justifyContent:"center"
                        }}
                        value={this.state.showData}
                        useNativeAndroidPickerStyle={false}
                        textInputProps={{ underlineColor: "#3B5998" }}
                        Icon={() => {
                          return (
                            <Image style={{width:20, height:10, resizeMode:"contain"}} source={require("../../assets/caret-down.png")}/>
                          );
                        }}
                      />
                      </View>
                    </View>
                     </View>
                     {/* ................... */}
                     {this.state.showData == "Ladder #9" || this.state.showData == "Humble Bee" || this.state.showData == "Western Provisions" ? (
                      <View style={{ width:"100%", marginTop: 5, alignContent:"center", alignItems:"center" }}>
                    <View style={{ width: "95%", paddingRight: 10 }}>
                      <View>
                        <Text style={{ color: "#ffffff",fontSize: 16, fontWeight: "bold" }}>
                          Type
                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                        
                        <RNPickerSelect
                        placeholder={{}}
                        items={[
                { label: 'Wine', value: 'Wine' },
                { label: 'Spirit', value: 'Spirit' },
                { label: 'Campagne/Sparkling', value: 'Campagne/Sparkling' },
               
            ]}
                        onValueChange={this.onTypeSelect.bind(this)}
                        style={{
                          ...smallpickerSelectStyles,
                          iconContainer: {
                            top: 15,
                            right: 15
                          },
                          width: 30,
                          justifyContent:"center"
                        }}
                        value={this.state.type}
                        useNativeAndroidPickerStyle={false}
                        textInputProps={{ underlineColor: "#3B5998" }}
                        Icon={() => {
                          return (
                            <Image style={{width:20, height:10, resizeMode:"contain"}} source={require("../../assets/caret-down.png")}/>
                          );
                        }}
                      />
                      </View>
                    </View>
                    <View style={{ width: "95%", paddingRight: 10 }}>
                      <View>
                        <Text style={{ color: "#ffffff",fontSize: 16, fontWeight: "bold" }}>
                          Bottle Size
                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                        
                        <RNPickerSelect
                        placeholder={{}}
                        items={[
                { label: '0.375', value: '0.375' },
                { label: '0.750', value: '0.750' },
                { label: '1.000', value: '1.000' },
                { label: '1.750', value: '1.750' },
               
            ]}
                        onValueChange={this.onBottleSizeSelect.bind(this)}
                        style={{
                          ...smallpickerSelectStyles,
                          iconContainer: {
                            top: 15,
                            right: 15
                          },
                          width: 30,
                          justifyContent:"center"
                        }}
                        value={this.state.bottle_size}
                        useNativeAndroidPickerStyle={false}
                        textInputProps={{ underlineColor: "#3B5998" }}
                        Icon={() => {
                          return (
                            <Image style={{width:20, height:10, resizeMode:"contain"}} source={require("../../assets/caret-down.png")}/>
                          );
                        }}
                      />
                      </View>
                    </View>
                    <View style={{ width: "95%", paddingRight: 10, flexDirection:"row" }}>
                    <View style={{width:"50%"}}>
                    <View>
                        <Text style={{ color: "#ffffff",fontSize: 16, fontWeight: "bold" }}>
                          ABV %
                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                        
                        <Input 
                        style={{color:"#fff"}} 
                        value={this.state.abv}
                        onChangeText={abv => this.setState({ abv })}
                         />
                      </View>
                   
                    </View>
                    <View style={{width:"50%"}}>
                    <View>
                        <Text style={{ color: "#ffffff",fontSize: 16, fontWeight: "bold" }}>
                        Retail Price/Bottle

                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                        
                        <Input 
                        style={{color:"#fff"}} 
                        value={this.state.retail_price_p_bottle}
                        onChangeText={retail_price_p_bottle => this.setState({ retail_price_p_bottle })}
                         />
                      </View>
                   
                    </View>
                   
                      </View>

                      <View style={{ width: "95%", paddingRight: 10, flexDirection:"row" }}>
                    <View style={{width:"50%"}}>
                    <View>
                        <Text style={{ color: "#ffffff",fontSize: 16, fontWeight: "bold" }}>
                          Case Size
                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                        
                        <Input 
                        style={{color:"#fff"}} 
                        value={this.state.case_size}
                        onChangeText={case_size => this.setState({ case_size })}
                         />
                      </View>
                   
                    </View>
                    <View style={{width:"50%"}}>
                    <View>
                        <Text style={{ color: "#ffffff",fontSize: 16, fontWeight: "bold" }}>
                        Retail Margin

                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                        
                        <Input 
                        style={{color:"#fff"}} 
                        value={this.state.retail_margin}
                        onChangeText={retail_margin => this.setState({ retail_margin })}
                         />
                      </View>
                   
                    </View>
                    {/* <View style={{width:"50%"}}>
                    <View>
                        <Text style={{ color: "#ffffff",fontSize: 16, fontWeight: "bold" }}>
                        No. of Pallets

                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                        
                        <Input 
                        style={{color:"#fff"}} 
                        value={this.state.no_of_pallet}
                        onChangeText={no_of_pallet => this.setState({ no_of_pallet })}
                         />
                      </View>
                   
                    </View> */}
                   
                      </View>
                     
                      <View style={{ width: "95%", paddingRight: 10, flexDirection:"row" }}>
                    {/* <View style={{width:"50%"}}>
                    <View>
                        <Text style={{ color: "#ffffff",fontSize: 16, fontWeight: "bold" }}>
                          Cases/Pallet
                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                        
                        <Input 
                        style={{color:"#fff"}} 
                        value={this.state.case_per_pallet}
                        onChangeText={case_per_pallet => this.setState({ case_per_pallet })}
                         />
                      </View>
                   
                    </View> */}
                    
                   
                      </View>
                     
                      <View style={{ width: "95%", paddingRight: 10, flexDirection:"row" }}>
                    <View style={{width:"50%"}}>
                    <View>
                        <Text style={{ color: "#ffffff",fontSize: 16, fontWeight: "bold" }}>
                         Distributor Margin
                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                        
                        <Input 
                        style={{color:"#fff"}} 
                        value={this.state.distributor_margin}
                        onChangeText={distributor_margin => this.setState({ distributor_margin })}
                         />
                      </View>
                   
                    </View>
                    <View style={{width:"50%"}}>
                    <View>
                        <Text style={{ color: "#ffffff",fontSize: 16, fontWeight: "bold" }}>
                        Domestic Transport

                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                        
                        <Input 
                        style={{color:"#fff"}} 
                        value={this.state.domestic_transportation}
                        onChangeText={domestic_transportation => this.setState({ domestic_transportation })}
                         />
                      </View>
                   
                    </View>
                   
                      </View>
                    
                      {/* <View style={{ width: "95%", paddingRight: 10 }}>
                    <View>
                        <Text style={{ color: "#ffffff",fontSize: 16, fontWeight: "bold" }}>
                        A&P Support
                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                        
                        <Input 
                        disabled={this.state.showData == "Humble Bee" ? 'true' : 'false'}
                        style={{color:"#fff"}} 
                        value={this.state.ap_support}
                        onChangeText={ap_support => this.setState({ ap_support })}
                         />
                      </View>
                   
                    
                   
                      </View>
                       */}
                     </View>
                 
                     ) : (
                      <View style={{ width:"100%", marginTop: 5, alignContent:"center", alignItems:"center" }}>
                    <View style={{ width: "95%", paddingRight: 10 }}>
                      <View>
                        <Text style={{ color: "#ffffff",fontSize: 16, fontWeight: "bold" }}>
                          Type
                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                        
                        <RNPickerSelect
                        placeholder={{}}
                        items={[
                { label: 'Wine', value: 'Wine' },
                { label: 'Spirit', value: 'Spirit' },
                { label: 'Campagne/Sparkling', value: 'Campagne/Sparkling' },
               
            ]}
                        onValueChange={this.onTypeSelect.bind(this)}
                        style={{
                          ...smallpickerSelectStyles,
                          iconContainer: {
                            top: 15,
                            right: 15
                          },
                          width: 30,
                          justifyContent:"center"
                        }}
                        value={this.state.type}
                        useNativeAndroidPickerStyle={false}
                        textInputProps={{ underlineColor: "#3B5998" }}
                        Icon={() => {
                          return (
                            <Image style={{width:20, height:10, resizeMode:"contain"}} source={require("../../assets/caret-down.png")}/>
                          );
                        }}
                      />
                      </View>
                    </View>
                    <View style={{ width: "95%", paddingRight: 10 }}>
                      <View>
                        <Text style={{ color: "#ffffff",fontSize: 16, fontWeight: "bold" }}>
                          Bottle Size
                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                        
                        <RNPickerSelect
                        placeholder={{}}
                        items={[
                { label: '0.500', value: '0.500' },
                { label: '0.100', value: '0.100' },
                { label: '0.200', value: '0.200' },
                { label: '0.375', value: '0.375' },
                { label: '0.750', value: '0.750' },
                { label: '1.000', value: '0.000' },
                { label: '1.750', value: '0.750' },
               
            ]}
                        onValueChange={this.onBottleSizeSelect.bind(this)}
                        style={{
                          ...smallpickerSelectStyles,
                          iconContainer: {
                            top: 15,
                            right: 15
                          },
                          width: 30,
                          justifyContent:"center"
                        }}
                        value={this.state.bottle_size}
                        useNativeAndroidPickerStyle={false}
                        textInputProps={{ underlineColor: "#3B5998" }}
                        Icon={() => {
                          return (
                            <Image style={{width:20, height:10, resizeMode:"contain"}} source={require("../../assets/caret-down.png")}/>
                          );
                        }}
                      />
                      </View>
                    </View>
                    <View style={{ width: "95%", paddingRight: 10, flexDirection:"row" }}>
                    <View style={{width:"50%"}}>
                    <View>
                        <Text style={{ color: "#ffffff",fontSize: 16, fontWeight: "bold" }}>
                          ABV %
                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                        
                        <Input 
                        style={{color:"#fff"}} 
                        value={this.state.abv}
                        onChangeText={abv => this.setState({ abv })}
                         />
                      </View>
                   
                    </View>
                    <View style={{width:"50%"}}>
                    <View>
                        <Text style={{ color: "#ffffff",fontSize: 16, fontWeight: "bold" }}>
                        Retail Price/Bottle

                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                        
                        <Input 
                        style={{color:"#fff"}} 
                        value={this.state.retail_price_p_bottle}
                        onChangeText={retail_price_p_bottle => this.setState({ retail_price_p_bottle })}
                         />
                      </View>
                   
                    </View>
                   
                      </View>

                      <View style={{ width: "95%", paddingRight: 10, flexDirection:"row" }}>
                    <View style={{width:"50%"}}>
                    <View>
                        <Text style={{ color: "#ffffff",fontSize: 16, fontWeight: "bold" }}>
                          Case Size
                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                        
                        <Input 
                        style={{color:"#fff"}} 
                        value={this.state.case_size}
                        onChangeText={case_size => this.setState({ case_size })}
                         />
                      </View>
                   
                    </View>
                    <View style={{width:"50%"}}>
                    <View>
                        <Text style={{ color: "#ffffff",fontSize: 16, fontWeight: "bold" }}>
                        Retail Margin

                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                        
                        <Input 
                        style={{color:"#fff"}} 
                        value={this.state.retail_margin}
                        onChangeText={retail_margin => this.setState({ retail_margin })}
                         />
                      </View>
                   
                    </View>
                    {/* <View style={{width:"50%"}}>
                    <View>
                        <Text style={{ color: "#ffffff",fontSize: 16, fontWeight: "bold" }}>
                        No. of Pallets

                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                        
                        <Input 
                        style={{color:"#fff"}} 
                        value={this.state.no_of_pallet}
                        onChangeText={no_of_pallet => this.setState({ no_of_pallet })}
                         />
                      </View>
                   
                    </View> */}
                   
                      </View>
                     
                      <View style={{ width: "95%", paddingRight: 10, flexDirection:"row" }}>
                    {/* <View style={{width:"50%"}}>
                    <View>
                        <Text style={{ color: "#ffffff",fontSize: 16, fontWeight: "bold" }}>
                          Cases/Pallet
                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                        
                        <Input 
                        style={{color:"#fff"}} 
                        value={this.state.case_per_pallet}
                        onChangeText={case_per_pallet => this.setState({ case_per_pallet })}
                         />
                      </View>
                   
                    </View> */}
                    
                   
                      </View>
                     
                      <View style={{ width: "95%", paddingRight: 10, flexDirection:"row" }}>
                    <View style={{width:"50%"}}>
                    <View>
                        <Text style={{ color: "#ffffff",fontSize: 16, fontWeight: "bold" }}>
                         Distributor Margin
                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                        
                        <Input 
                        style={{color:"#fff"}} 
                        value={this.state.distributor_margin}
                        onChangeText={distributor_margin => this.setState({ distributor_margin })}
                         />
                      </View>
                   
                    </View>
                    <View style={{width:"50%"}}>
                    <View>
                        <Text style={{ color: "#ffffff",fontSize: 16, fontWeight: "bold" }}>
                        Domestic Transport

                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                        
                        <Input 
                        style={{color:"#fff"}} 
                        value={this.state.domestic_transportation}
                        onChangeText={domestic_transportation => this.setState({ domestic_transportation })}
                         />
                      </View>
                   
                    </View>
                   
                      </View>
                    
                      {/* <View style={{ width: "95%", paddingRight: 10 }}>
                    <View>
                        <Text style={{ color: "#ffffff",fontSize: 16, fontWeight: "bold" }}>
                        A&P Support
                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                        
                        <Input 
                        style={{color:"#fff"}} 
                        disabled={isAPdisabled}
                        value={this.state.ap_support}
                        onChangeText={ap_support => this.setState({ ap_support })}
                         />
                      </View>
                   
                    
                   
                      </View>
                       
                       
                       <View style={{ width: "95%", paddingRight: 10}}>
                    <View>
                        <Text style={{ color: "#ffffff",fontSize: 16, fontWeight: "bold" }}>
                       PBC Cost of Good Sold
                        </Text>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          borderWidth: 1.2,
                          borderRadius: 10,
                          borderColor: "#ffffff",
                          padding: 0,
                          marginTop: 10
                        }}
                      >
                        
                        <Input 
                        style={{color:"#fff"}} 
                        value={this.state.pbc}
                        onChangeText={pbc => this.setState({ pbc })}
                         />
                      </View>
                   
                    
                   
                      </View>
                     */}
                     </View>
                     )}
<View style={{width:"100%", alignContent:"center", alignItems:"center", marginTop:30}}>
  <View style={{width:"50%"}}>
  {this.state.loading ? (
    <Button
                    bordered
                    // onPress={this.calculate}
                    style={{
                      width: "100%",
                      alignItems: "center",
                      justifyContent: "center",
                      borderColor: "#fff",
                      borderRadius: 5,
                      backgroundColor: "transparent"
                    }}
                  >
                  <ActivityIndicator size={"small"} />
                  </Button>
    
  ) : (
    <Button
                    bordered
                    onPress={this.calculate}
                    style={{
                      width: "100%",
                      alignItems: "center",
                      justifyContent: "center",
                      borderColor: "#fff",
                      borderRadius: 5,
                      backgroundColor: "transparent"
                    }}
                  >
                    <Text
                      style={{
                        textAlign: "center",
                        color: "#e8eaf6",
                        fontSize: 17,
                        fontWeight:"bold"
                      }}
                    >
                      Calculate
                    </Text>
                  </Button>
  )}

  </View>
</View>
                    </View>
              </View>
            </ScrollView>
          )}
        </View>
      
    </View>
       
    );
  }
}
const smallpickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 8,
    paddingLeft: 7,
   
    color: "#fff",
    width: "100%"
    // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 16,
    paddingVertical: 8,
    paddingLeft: 7,
    
    color: "#fff",
    width: "100%" // to ensure the text is never behind the icon
  }
});
export default UnitEconomics;
