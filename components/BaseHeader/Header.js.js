import React, { Component } from "react";
import { Header, Button, Left, Right, Icon, Container, Form, Textarea } from "native-base";
// import { inject } from "mobx-react/native"; 
// import IconBadge from "react-native-icon-badge";
import {
  Dimensions,
  Platform,
  StatusBar,
  Modal,
  Text,
  TouchableHighlight,
  View,
  Alert,
  TouchableOpacity,
  Image
} from "react-native";
import Constants from 'expo-constants'
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

// @inject("routerActions")
export default class BaseHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
    this.navigate = this.props.navigation.navigate;
  }
 
  render() {
    const navigation = this.props.navigation;
    return (
      <View style={{width:deviceWidth}}>
<View style={{flexDirection:"row", width:deviceWidth}}>
<View style={{width:"30%"}}>

<View style={{flexDirection:"row", width:"100%"}}>

{/* {this.props.back ? (
  <TouchableOpacity onPress={()=> this.props.navigation.goBack()} style={{paddingLeft:20, marginTop:25}}>
<Image style={{width:30, height:30, resizeMode:"contain"}} source={require('../../assets/images/back.png')}/>
</TouchableOpacity>
) : (
  <View style={{paddingLeft:20, marginTop:25}}>
<Text style={{fontSize:30, fontWeight:"bold", color:"#fff"}}>{this.props.title}</Text>
</View>
)} */}
{this.props.back ? (
  <TouchableOpacity onPress={()=> this.props.navigation.goBack()} style={{paddingLeft:20, marginTop:25}}>
<Image style={{width:30, height:30, resizeMode:"contain"}} source={require('../../assets/images/back.png')}/>
</TouchableOpacity>
) : null}

</View>

</View>
<View style={{width:"70%", alignContent:"center", alignItems:"center",justifyContent:"center"}}>
{this.props.logo ? (
<View style={{width:"80%",paddingRight:5, paddingTop:20}}>
<Image style={{width:"100%", height:80, resizeMode:"contain"}} source={require("../../assets/images/head_img.png")}/>
</View>

):null}

</View>
</View>
{/* <TouchableHighlight
onPress={() => this.props.navigation.goBack()}
>
<Text style={{color:"#82a601", fontSize:20}}>Back</Text>

</TouchableHighlight> */}
<View style={{width:"100%", alignContent:"center", alignItems:"center"}}>
  <View style={{width:"70%", borderBottomWidth:0.8, borderBottomColor:"#EAEAEA", marginTop:10}}></View>
</View>
      </View>
     );
  }
}


